![Alt text](http://www.stccmop.org/files/images/selfe-icon-blue.png)

#SELFE

SELFE is an open-source community-supported code. 
It is based on unstructured triangular grids, and designed for the effective simulation of 3D baroclinic circulation. 
Originally developed to meet stringent modeling challenges for the Columbia River estuary and plume, SELFE has now been extensively applied to study circulation in coastal margins around the world. 
Several related codes constitute an interdisciplinary modeling system that is growing organically around SELFE.

SELFE uses a semi-implicit finite-element/volume Eulerian-Lagrangian algorithm to solve the Navier-Stokes equations (in either hydrostatic and non-hydrostatic form). 
The numerical algorithm is low-order, robust, stable and computationally efficient. 
It also naturally incorporates wetting and drying of tidal flats.

## Application areas
- Cross-scale river-to-ocean circulation
- Tsunami hazards
- Storm surge
- Sediment transport
- Ecology
- Oil spill

See: http://www.stccmop.org/knowledge_transfer/software/selfe

## Usage
See: http://www.stccmop.org/knowledge_transfer/software/selfe/documentation

## License

SELFE is released under the GNU General Public License, version 2.

See: COPYING.txt