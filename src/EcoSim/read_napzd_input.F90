    
    SUBROUTINE read_napzd_input

        
!!======================================================================
!! June, 2007                                                          ! 
!!======================================================Marta Rodrigues=
!!                                                                     !
!! This subroutine reads ecological models inputs                      !
!!                                                                     ! 
!!======================================================================	 

!
!      USE bio_param
!      USE global   
      USE biology_napzd
      USE elfe_msgp, only : myrank,parallel_abort

!
      IMPLICIT NONE
      SAVE  

! Local variables

      CHARACTER(len=100) :: var1, var2
      INTEGER :: i, j, ierror      
      
      OPEN(5, file='napzd_spitz.in', status='old')
!Error: error=ierror needed in open()? 
      IF(ierror/=0) THEN
        call parallel_abort('Error opening ecological model input parameters (napzd_spitz.in)!')
!        STOP
      END IF
      
! Reads input
     READ(5,*) var1, var2, AttSW
     READ(5,*) var1, var2, AttPhyto
     READ(5,*) var1, var2, PARfrac
     READ(5,*) var1, var2, Vp0  
     READ(5,*) var1, var2, K_u
     READ(5,*) var1, var2, omega_bio
     READ(5,*) var1, var2, psi_bio
     READ(5,*) var1, var2, alpha_bio
     READ(5,*) var1, var2, theta_bio
     READ(5,*) var1, var2, phi_bio
     READ(5,*) var1, var2, rm_bio
     READ(5,*) var1, var2, lambda_bio 
     READ(5,*) var1, var2, PhytO
     READ(5,*) var1, var2, wst_bio
     READ(5,*) var1, var2, gamma_bio
     READ(5,*) var1, var2, wDetn
     READ(5,*) var1, var2, wPhyt
!     READ(5,*) var1, var2, (TNU2(i), i=1,NBT)
!     READ(5,*) var1, var2, (TNU4(i), i=1,NBT)
!     READ(5,*) var1, var2, (AKT_BAK(i), i=1,NBT)
!     READ(5,*) var1, var2, (TNUDG(i), i=1,NBT)
!     READ(5,*) var1, var2, (Hout(idTvar)(i), i=1,NBT)
         
      CLOSE(5)     

!  set up the idbio indexes
       do i=1,NBT
          idbio(i)=i
       enddo
 
      END SUBROUTINE read_napzd_input
