     SUBROUTINE sed_init
!
!!======================================================================
!! August, 2007                                                        !
!!==========================================================Ligia Pinto!
!!                                                                     !
!! This subroutine is from ROMS (where is called ana_sediment.h)       !
!!                                                                     !
!!======================================================================
!!
!!======================================================================
!! Copyright (c) 2002-2007 The ROMS/TOMS Group                         !
!!   Licensed under a MIT/X style license                              !
!!   See License_ROMS.txt                                              !
!!                                                                     !
!=======================================================================
!                                                                      !
!  This routine sets initial conditions for  sedimen t tracer fields   !
!  concentrations  (kg/m3) using analytical expressions for sediment   !
!  and/or bottom boundary layer configurations. It also sets initial   !
!  bed conditions in each sediment layer.                              !
!                                                                      !
!=======================================================================
!

       USE sed_mod
       USE elfe_glbl, only: nea,dt,ntracers,nm,idry_e,errmsg,area
       USE elfe_msgp, only: myrank,parallel_abort

       IMPLICIT NONE

       integer :: i, ised, j, k, kbed
       integer :: node1,node2,node3

       real(r8), dimension(nea) :: bed_thick0
       real(r8) :: cff1, cff2, cff3, cff4, Kvisc
       real(r8) :: bed_frac_sum(Nbed,nea),bed_frac_tmp(Nbed,nea,ntracers)

!--------------
! start statement
!--------------
       if(myrank==0) write(16,*)'Entering sed_init'

!      time=dt

!----------------------------------------------------------------
! Each cell of each layer in the bed is initialized
! with a thickness, sediment-class distribution, porosity and age
!----------------------------------------------------------------
       if(myrank==0) write(16,*)'Initialize bed parameters'

       do i=1,Nbed
         do j=1,nea
!           bed(i,j,ithck)=0.1_r8
           bed(i,j,ithck)=bedthick_overall
           bed(i,j,iaged)=0.0_r8
!           bed(i,j,iporo)=0.9_r8
           bed(i,j,iporo)=porosity
         enddo !j
       enddo !i

!--------------------------------------
!bed fractions sum check
!sum of bed fractions must not exceed 1
!less than 1 is ok I guess
!--------------------------------------
       if(myrank==0) write(16,*)'Initialize bed fractions'

       bed_frac_sum=0 !initialize variable
       do i=1,Nbed
         do j=1,nea
           do k=1,ntracers
             !save bed fraction info in tmp-variable
             bed_frac_tmp(i,j,k)=bed_frac(i,j,k)
             bed_frac_sum(i,j)=bed_frac_sum(i,j)+bed_frac(i,j,k)
           enddo !k
			
           if (bed_frac_sum(i,j).GT.1) then
             write(errmsg,*)'sum of bed fractions is greater than 1 at node ',j
             call parallel_abort(errmsg)
           end if

         enddo !j
       enddo !i

!----------------------------------------------------------------------------------
!initial bed fractions are read in for nodes and thus need to be mapped to elements
!regardless of the bottom cell being dry or not
       bed_frac=0 !wipe variable, info is currently also in tmp variable
!----------------------------------------------------------------------------------
       if(myrank==0) write(16,*)'map bed fractions'

       do i=1,Nbed
         do j=1,nea !loop over all elements in bottom layer
           node1=nm(j,1) !get index of first node of element
           node2=nm(j,2) !get index of second node of element
           node3=nm(j,3) !get index of third node of element

           !map from nodes to elements by simply doing sum/3
           do k=1,ntracers
             bed_frac(i,j,k)=(bed_frac_tmp(i,node1,k)+bed_frac_tmp(i,node2,k)+bed_frac_tmp(i,node3,k))/3
           enddo !ntracers
         enddo !nea
       enddo !Nbed

!-----------------------------------------------------------------------
!  If only bottom boundary layer and not sediment model, set bottom
!  sediment grain diameter (m) and density (kg/m3).
!-----------------------------------------------------------------------
!
!      DO j=1,nea
!!          bottom(j,isd50)=0.0003


!       bottom(j,isd50)=0.000140_r8  !vanrijn
!!          bottom(j,isd50)=0.000042_r8
!       bottom(j,idens)=2650.0_r8
!      END DO
!      DO j=1,nea
!       bottom(j,itauc)=0.15_r8/rhom !vanrijn
!!          bottom(j,itauc)=0.2_r8/rhom
!      END DO
!
!-----------------------------------------------------------------------
!  If only Blass bottom boundary layer and not sediment model, set
!  sediment settling velocity (m/s).
!-----------------------------------------------------------------------
!
!      Kvisc=0.0013_r8/rhom
      
!      DO j=1,nea
!          bottom(j,iwsed)=0.011_r8  !vanrijn
!          bottom(j,iwsed)=0.038_r8
!!
!! Consider Souslby (1997) estimate of settling velocity.
!!
!!        D=bottom(j,isd50)*g*                                        &
!!   &      ((bottom(j,idens)/rhom-1.0)/Kvisk)**(1.0_r8/3.0_r8)
!!        bottom(j,iwsed)=Kvisc*(SQRT(10.36_r8*10.36_r8+
!!   &                      1.049_r8*D*D*D)-10.36_r8)/bottom(j,isd50)
!      END DO

!-----------------------------------------------------------------------
!  Initial sediment bed layer properties of age, thickness, porosity,
!  and initialize sediment bottom properites of ripple length, ripple
!  height, and default Zob.
!-----------------------------------------------------------------------
!
!      DO j=1,nea
!        DO k=1,Nbed
!          bed(k,j,iaged)=dt
!          bed(k,j,ithck)=10.0_r8
!          bed(k,j,iporo)=0.40_r8
!          DO ised=1,ntracers
!             bed_frac(k,j,ised)=1.0_r8/FLOAT(ntracers)
!          ENDDO
!        END DO !k

!  Set exposed sediment layer properties.

!        bottom(j,irlen)=0.0_r8
!        bottom(j,irhgt)=0.0_r8
!        bottom(j,izdef)=Zob(j)
!      END DO !j


!-------------------------------
! Initial sediment bed_mass [kg/m2]
!-------------------------------
      DO k=1,Nbed
        DO j=1,nea
          DO ised=1,ntracers
               bed_mass(k,j,1,ised)= bed(k,j,ithck)*&
!                                   & area(j)*&
                                   & Srho(ised)*&
                                   & (1.0_r8-bed(k,j,iporo))*&
                                   & bed_frac(k,j,ised)
             
          END DO
        END DO
      END DO
! 
!  Set exposed sediment layer properties.
!
      DO j=1,nea
          cff1=1.0_r8
          cff2=1.0_r8
          cff3=1.0_r8
          cff4=1.0_r8

!------------------------
! weighted geometric mean
!------------------------
          DO ised=1,ntracers
            cff1=cff1*Sd50(ised)**bed_frac(1,j,ised)
            cff2=cff2*Srho(ised)**bed_frac(1,j,ised)
            cff3=cff3*Wsed(ised)**bed_frac(1,j,ised)
            cff4=cff4*tau_ce(ised)**bed_frac(1,j,ised)
          END DO

          bottom(j,isd50)=cff1
          bottom(j,idens)=cff2
          bottom(j,iwsed)=cff3
          bottom(j,itauc)=cff4
      END DO
!      if(myrank==0) write(16,*)'antes total'

!#if defined SED_MORPH
!
!-----------------------------------------------------------------------
!  Compute initial total thickness for all sediment bed layers.
!-----------------------------------------------------------------------
!
!            DO j=1,nea
!               bed_thick0(j)=0.d0
!               DO kbed=1,Nbed
!                  bed_thick0(j)=bed_thick0(j)+bed(kbed,j,ithck)
!               END DO
!               bed_thick(j,1)=bed_thick0(j)
!               bed_thick(j,2)=bed_thick0(j)
!            END DO

!       bed_thick0=0.d0

!            DO i=1,Nbed
               
!               DO j=1,nea
!                  bed_thick0(j)=0.d0
!                  bed_thick0(j)=bed_thick0(j)+bed(i,j,ithck)
!               END DO
!               bed_thick(j,1)=bed_thick0(j)
!               bed_thick(j,2)=bed_thick0(j)
!            END DO

!--------------------------------------------------------
! compute total bed thickness
! by summing over all layer thicknesses
!--------------------------------------------------------
       bed_thick=0.0_r8 !initialization

       do i=1,Nbed
         do j=1,nea
           bed_thick(j)=bed_thick(j)+bed(i,j,ithck)
         end do !nea
       end do !Nbed

!#endif /*SED_MORPH*/

!----------------------
! sediment debug output
!----------------------
       if(sed_debug==1) then
         if(myrank==0) then
           do i=1,Nbed
             do j=1,nea
               do k=1,ntracers
                 write(16,'(A,I1,A,I5,A,I1,A,F11.8)')'Nbed:',i,' nea:',j,' ntracers:',k,' bed_frac.ic:',bed_frac(i,j,k)
               enddo !ntracers
             enddo !nea
           enddo !Nbed

           do i=1,Nbed
             do j=1,nea
               write(16,'(A,I1,A,I5,A,F11.8,A,F11.8,A,F11.8,A,F11.8)')'Nbed:',i,' nea:',j,' bed_thick:',bed_thick(j),' bed(ithck):',bed(i,j,ithck),' bed(iaged):',bed(i,j,iaged),' bed(iporo):',bed(i,j,iporo)
             enddo !nea
           enddo !Nbed

           do i=1,nea
             write(16,'(A,I5,A,F11.8,A,F11.8,A,F11.8,A,F11.8)')'nea:',i,' bottom(itauc):',bottom(i,itauc),' bottom(isd50):',bottom(i,isd50),' bottom(iwsed):',bottom(i,iwsed),' bottom(idens):',bottom(i,idens)
           enddo !nea

!           do i=1,nea
!             write(16,'(A,I5,A,E,A,E,A,E,A,E)')'nea:',i,' bottom(nea,tauc):',bottom(i,tauc),' bottom(nea,isd50):',bottom(i,isd50),' bottom(nea,iwsed):',bottom(i,iwsed),' bottom(nea,idens):',bottom(i,idens)
!           enddo !nea

         end if !myrank
       end if !sed_debug

!--------------
! end statement
!--------------
       if(myrank==0) write(16,*)'Leaving sed_init'

       RETURN
       END SUBROUTINE sed_init
