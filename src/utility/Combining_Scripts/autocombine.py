#!/usr/bin/env python
"""
Autocombine script for necdf files combine_output7.

Tuomas Karna 2013-02-15
"""
from optparse import OptionParser
import subprocess as sub
import shutil
import glob
import sys
import os
import time

import numpy as np
from netCDF4 import Dataset as NetCDFFile

cmd = 'combine_output7'
# same order as SELFE writes the files
fileNames=['elev.61','pres.61','airt.61','shum.61','srad.61','flsu.61',
           'fllu.61','radu.61','radd.61','flux.61','evap.61','prcp.61',
           'wind.62','wist.62','dahv.62','vert.63','temp.63','salt.63',
           'conc.63','tdff.63','vdff.63','kine.63','mixl.63','zcor.63',
           'qnon.63','totN.63','hvel.64','hvel.67','vert.69','temp.70',
           'salt.70','dihv.71','dihv.66','Diag.63','Bbdf.63','nem.63',
           'salt_flux.68', 'temp_flux.68',
           'di_salt_flux.65', 'di_temp_flux.65']

def createDirectory(path) :
  if os.path.exists(path) :
    if not os.path.isdir(path) :
      raise Exception( 'file with same name exists',path )
  else :
    os.makedirs(path)
  return path

def appendTracerFiles( fileNames, nTracers ) :
  fn = list(fileNames)
  for i in range(nTracers) :
    fn.append( 'trcr_'+str(i+1)+'.63' )
    fn.append( 'trcr_'+str(i+1)+'.70' )
    fn.append( 'trcr_'+str(i+1)+'_flux.68')
    fn.append( 'di_trcr_'+str(i+1)+'_flux.65')
  return fn

def appendSedFiles( fileNames, nTracers ) :
  fn = list(fileNames)
  for i in range(nTracers) :
    fn.append( 'bedlv_'+str(i+1)+'.61' )
    fn.append( 'bedlu_'+str(i+1)+'.61' )
  return fn


  trcr_1 = '%s/%d_trcr_1.63.nc' % (outDir, stack)
  turbid = '%s/%d_turbidity.63.nc' % (outDir, stack)
  if not os.path.isfile( trcr_1 ) :
    raise Exception('Tracer file not found, not combinded?',trcr_1)
  shutil.copyfile(trcr_1, turbid) 
  turbidity = NetCDFFile(turbid, 'a')
  turbidity.renameVariable('trcr_1', 'turbidity')
  t = turbidity.variables['turbidity']
  t.setncattr('long_name', 'turbidity')
  d = turbidity.variables['turbidity'][:,:,:]
  for s_class in range(2, nTracers+1) :
    trcr = '%s/%d_trcr_%d.63.nc' % (outDir, stack, s_class)
    s_data = NetCDFFile(trcr).variables['trcr_%d' % s_class][:,:,:]
    d = d + s_data
  if normalize :
    d = d/np.max(d)
  turbidity.variables['turbidity'][:,:,:] = d
  turbidity.sync()
  turbidity.close()

def checkDataDir( dataDir, daemon=False ) :
  """Checks that dataDir exists and as all the local_to_global_* files"""
  nProc,nTrac = parseLocalGlobalFile(dataDir, daemon)
  for iProc in range(nProc) :
    checkLocalGlobalFile(dataDir,iProc, daemon)
  return

def checkLocalGlobalFile(dataDir,iProc=0, daemon=False) :
  """Checks if local_to_global_[iProc] file exists.
  If daemon=True, waits for it to be written."""
  f = os.path.join(dataDir,'local_to_global_{0:04d}'.format(iProc))
  if daemon :
    for i in range(10) :
      if not os.path.isfile( f ) :
        print 'waiting for',f
        sys.stdout.flush()
        time.sleep(300)
      else :
        return f
  if not os.path.isfile( f ) :
    raise Exception( 'Could not find file: '+f )
  return f

def parseLocalGlobalFile(dataDir, daemon=False) :
  """Parses number of processors and number of tracers from
  local_to_global_0000 file."""
  f = checkLocalGlobalFile(dataDir,iProc=0,daemon=daemon)
  locFile = open(f)
  words = locFile.readline().split()
  nProc = int(words[3])
  nTrac = int(words[4])
  return nProc,nTrac

def checkSize( file, targetSize ) :
  """Returns True it file size matches targetSize. If targetSize==0, will check for nonzero size."""
  if not os.path.isfile( file ) :
    return False
  if targetSize > 0 :
    return os.path.getsize(file) == targetSize
  else :
    return os.path.getsize(file) > 0

def checkNetCDFDimensions( file, nTimeSteps ) :
  """Checks given netcdf file for correct dimensions.
  Variable name is assumed to be same as in filename,
  appended with _{x,y} for vectors."""
  ncfile = NetCDFFile(file)
  varName = os.path.split(file)[1].split('.')[0].split('_')[1]
  for var in ncfile.variables.keys() :
    if var in [varName,varName+'_x',varName+'_y'] :
      goodShape = ( (not 0 in ncfile.variables[var].shape) and
                    ncfile.variables[var].shape[0]==nTimeSteps )
      if not goodShape :
        print var, ncfile.variables[var].shape
        raise Exception( 'Bad variable dimension in file '+file )
      else :
        print var, 'dimension check OK'

def callCombine( cmd, fname, stack, dataDir, outDir, binaryOut,
                 nTimeSteps, round2Digit=None ) :
  """Calls combine_output routine, checks that all stored netcdf files have
  nTimeSteps time steps."""
  ncFlag = str(int( not binaryOut ))
  call = [cmd,fname,str(stack),str(stack),ncFlag,dataDir,outDir]
  if round2Digit :
    call.append( str(round2Digit) )
  s = sub.Popen(call,stderr=sub.STDOUT)
  ret = s.wait()
  if ret != 0 :
    raise Exception( 'combining failed :'+' '.join(call)+' returned code '+str(ret) )
  if not binaryOut :
    ncfname = os.path.join(outDir,'{0:d}_{1:s}.nc'.format(stack,fname))
    print 'checking file',ncfname
    checkNetCDFDimensions( ncfname, nTimeSteps )

import multiprocessing
# NOTE this must not be a local function in runTasksInQueue
def launch_job( task ) :
  """Excecutes a task"""
  function, args = task
  try :
    function( *args )
  except KeyboardInterrupt as e:
    raise e
  except Exception as e :
    print e

def runTasksInQueue( num_threads, tasks ) :
  """Generic routine for processing tasks in parallel.

  Tasks are defined as a (function,args) tuple, whick will is executed as
  function(*args).

  num_threads - (int) number of threads to use
  tasks - list of (function,args)
  """
  pool = multiprocessing.Pool(num_threads)
  p = pool.map_async(launch_job, tasks)
  timeLimit = 24*3600
  try:
    result = p.get(timeLimit)
  except KeyboardInterrupt:
    print ' Ctrl-c received! Killing...'

def combineStacks(dataDir,outDir,binaryOut,firstStack,lastStack,nTrac,sed,
                  nTimeSteps,round2Digit=None, num_threads=1) :
  """Combines all stacks in offline mode (all files must be present)"""
  allFileNames = appendTracerFiles( fileNames, nTrac )
  if sed : 
    allFileNames = appendSedFiles( allFileNames, nTrac )
  tasks = [] # list of all tasks to process
  for stack in range( firstStack, lastStack+1 ) :
    for fname in allFileNames :
      file = os.path.join(dataDir,'{0:d}_{1:04d}_{2:s}'.format(stack,0,fname))
      if not os.path.isfile(file) :
        print 'file not found, skipping',file
        continue
      function = callCombine
      args = [cmd, fname, stack, dataDir, outDir, binaryOut,
                   nTimeSteps, round2Digit]
      tasks.append( (function,args) )
    if sed :
      tasks.append( (makeTurbidity,[outDir, stack, nTrac]) )
  runTasksInQueue( num_threads, tasks )

def combineDaemon(dataDir,outDir,binaryOut,firstStack,lastStack,nTrac,sed,
                  nTimeSteps,round2Digit=None) :
  """Combines all stacks in daemon mode (waiting until files are written)"""
  nProc,nTrac = parseLocalGlobalFile(dataDir)
  allFileNames = appendTracerFiles( fileNames, nTrac )
  if sed : 
    allFileNames = appendSedFiles( allFileNames, nTrac )
  # construct a dict of uncombined file sizes
  uncombFileSize = {}
  for iProc in range(nProc) :
    for fname in allFileNames :
      uncombFileSize[(iProc,fname)] = 0
  for stack in range( firstStack, lastStack+1 ) :
    # check if {stack+1}_0000_elev.61 exists
    elevFile = os.path.join(dataDir,'{0:d}_{1:04d}_{2:s}'.format(stack+1,0,'elev.61'))
    while not os.path.isfile(elevFile) :
      # wait until exists
      print 'waiting for empty file',elevFile
      sys.stdout.flush()
      time.sleep(300)
    for fname in allFileNames :
      print fname
      # check that variable file exists for proc 0
      file = os.path.join(dataDir,'{0:d}_{1:04d}_{2:s}'.format(stack,0,fname))
      if not os.path.isfile(file) :
        # if not skip variable
        continue
      # check that file with correct size exists for all procs
      for iProc in range(nProc) :
        fproc = os.path.join(dataDir,'{0:d}_{1:04d}_{2:s}'.format(stack,iProc,fname))
        while not checkSize( fproc, uncombFileSize[(iProc,fname)] ) :
          # wait until written to disk
          print 'waiting for',fproc, os.path.getsize(fproc), uncombFileSize[(iProc,fname)]
          sys.stdout.flush()
          if uncombFileSize[(iProc,fname)] == 0 :
            time.sleep(300) # wait longer if correct size is unknown
          else :
            time.sleep(60)

      print 'processing',file
      sys.stdout.flush()
      callCombine( cmd, fname, stack, dataDir, outDir, binaryOut,
                   nTimeSteps, round2Digit)

      for iProc in range(nProc) :
        fproc = os.path.join(dataDir,'{0:d}_{1:04d}_{2:s}'.format(stack,iProc,fname))
        if uncombFileSize[(iProc,fname)] == 0:
          uncombFileSize[(iProc,fname)] = os.path.getsize(fproc)
          #print (iProc,fname), uncombFileSize[(iProc,fname)]

    if sed :
      print 'Creating %d_turbidity.63.nc' % stack
      makeTurbidity(outDir, stack, nTrac)

#-------------------------------------------------------------------------------
# Parse commandline arguments
#-------------------------------------------------------------------------------
if __name__=='__main__' :

  usage = ('Usage: %prog [options] firstStack lastStack\n')

  parser = OptionParser(usage=usage)
  parser.add_option('-o', '--outputDirectory', action='store', type='string',
                      dest='outDir', help='directory where combined files are stored (default %default)',default='outputs')
  parser.add_option('-d', '--dataDirectory', action='store', type='string',
                      dest='dataDir', help='directory where SELFE output files are (default %default)',default='outputs')
  parser.add_option('-B', '--binary-output', action='store_true',
                      dest='binaryOut', help='store outputs in SELFE binary format instead of netcdf (default %default)',default=False)
  parser.add_option('-D', '--daemon', action='store_true',
                      dest='daemonMode', help='run in daemon mode, waiting for uncombined files (default %default)',default=False)
  parser.add_option('-r', '--round-to-digits', action='store', type='int',
                      dest='round2Digit', help='number of significant digits to keep in data after rounding (optional, for netcdf only)')
  parser.add_option('-t', '--number-of-tracers', action='store', type='int',
                      dest='nTracers', help='number of tracers (default %default)',default=0)
  parser.add_option('-n', '--n-time-steps', action='store', type='int',
                      dest='nTimeSteps', help='correct number of time steps in each file for quality checking (default %default)',default=96)
  parser.add_option('-S', '--sediment', action='store_true',
                      dest='sediment', help='combine non-trcr_?.63 sediment model files (default %default)',default=False)
  parser.add_option('-j', '--numThreads', action='store', type='int',
                      dest='num_threads', help='Number of concurrent threads to use (default %default). Ignored for daemon mode.',default=1)
  (options, args) = parser.parse_args()

  if len(args) != 2 :
    parser.print_help()
    parser.error('could not parse firstStack, lastStack')
  firstStack = int(args[0])
  lastStack = int(args[1])

  outDir = options.outDir
  dataDir = options.dataDir
  binaryOut = options.binaryOut
  daemonMode = options.daemonMode
  round2Digit = options.round2Digit
  nTracers = options.nTracers
  nTimeSteps = options.nTimeSteps
  sed = options.sediment
  num_threads = options.num_threads

  print 'Parsed options:'
  print ' - stacks ', firstStack, lastStack
  print ' - dataDir ', dataDir
  print ' - outDir ', outDir
  print ' - netcdf ',not binaryOut
  print ' - wait for outputs ',daemonMode
  if round2Digit :
    print ' - round values to decimals ',round2Digit
  print ' - checking for n time steps ',nTimeSteps
  print ' - tracers ',nTracers
  if sed > 0 :
    print ' - including sediment specific files' 
  if num_threads > 1 :
    print ' - number of threads', num_threads 

  createDirectory(outDir)

  # check that dataDir contains local_to_global_0000
  checkDataDir(dataDir,daemonMode)

  if daemonMode :
    combineDaemon(dataDir,outDir,binaryOut,firstStack,lastStack,nTracers,sed,nTimeSteps,round2Digit)
  else :
    combineStacks(dataDir,outDir,binaryOut,firstStack,lastStack,nTracers,sed,nTimeSteps,round2Digit, num_threads)
