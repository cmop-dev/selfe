!===============================================================================
!Converts selfe binary output files to netcdf format.
!
!Outputs are XXXX.YY.nc, e.g. elev.61.nc
!In the netcdf container vector variables are stored as scalar components, e.g.
! hvel_x, hvel_y for hvel.63.

! In the case of sidecenter/center files (e.g. *.67, *.70), the nodes and
! connectivity matrix of the original grid is read from corresponding elev.61
! files.

! Usage:
! convSelfeBin2nc filename firststack laststack inetcdf datadir outdir [digits]
!
! Optional argument digits can be used to round all floats to n significant
! digits in order to save disk space.

! Example:
! convSelfeBin2nc salt.63 10 18 1 path/to/selfe/outputs/ some/path/
! convSelfeBin2nc salt.63 10 18 1 path/to/selfe/outputs/ some/path/ 4
!
!  Compile on head nodes:
! ifort -O2 -o convSelfeBin2nc convSelfeBin2nc.f90 selfe_geometry.f90 -assume byterecl -I/usr/local/include/ -L/usr/local/lib -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lm -lproj

!  Compile with gfortran:
! gfortran -O2 -g -o convSelfeBin2nc convSelfeBin2nc.f90 selfe_geometry.f90 -I/usr/local/include/ -L/usr/local/lib -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lm -lproj

!===============================================================================

module proj_bind
  !
  ! Module for interfacing between Proj-4 library libproj.a
  ! and Fortran. So far only pj_init_plus and pj_transform are supported.
  ! More routines can be added in a similar way.
  !
  use iso_c_binding
  implicit none
  private
  public :: pj_init_plus, pj_transform
  !
  interface
  function pj_init_plus(name) bind(c,name='pj_init_plus')
  use iso_c_binding
  implicit none
  type(c_ptr) :: pj_init_plus
  character(kind=c_char) :: name(*)
  end function pj_init_plus

  function pj_transform(src, dst, point_count, point_offset, &
  & x, y, z) bind(c,name='pj_transform')
  use iso_c_binding
  implicit none
  type(c_ptr), value :: src, dst
  integer(c_long), value :: point_count
  integer(c_int), value :: point_offset
  integer(c_int) :: pj_transform
  real(c_double) :: x, y, z
  end function pj_transform
  end interface
!
end module proj_bind

module convert_coords
  use iso_c_binding
  use proj_bind
  implicit none
  type(c_ptr) :: pj_ll, pj_wo, pj_spcs
  real(8), parameter :: RAD_TO_DEG = 180.0/3.14159265359
  real(8), parameter :: DEG_TO_RAD = 1.0/RAD_TO_DEG

  contains
    subroutine init_coord_sys()
      pj_spcs = pj_init_plus('+init=nad27:3601'//char(0))
      pj_ll = pj_init_plus('+proj=latlong +datum=WGS84'//char(0))
      pj_wo = pj_init_plus('+proj=latlong +nadgrids=WO'//char(0))
    end subroutine init_coord_sys
    subroutine convert_to_ll( x, y, z )
      implicit none
      real, intent(inout) :: x, y, z
      real(C_DOUBLE) xx,yy,zz
      integer(C_INT) res
      xx = x
      yy = y
      zz = z
      res = pj_transform(pj_spcs,pj_wo, 1_C_LONG, 0_C_INT, xx, yy, zz)
!       if ( res == 0 ) then
!         write(*,*) 'success',res,xx,yy
!       else
      if ( res /= 0 ) then
        write(*,*) 'failure',res,xx,yy
        res = pj_transform(pj_spcs,pj_ll, 1_C_LONG, 0_C_INT, xx, yy, zz)
      endif
      x = xx*RAD_TO_DEG
      y = yy*RAD_TO_DEG
      z = zz
    end subroutine convert_to_ll
    subroutine convert_to_spcs( x, y, z )
      implicit none
      real, intent(inout) :: x, y, z
      real(C_DOUBLE) xx,yy,zz
      integer(C_INT) res
      xx = x*DEG_TO_RAD
      yy = y*DEG_TO_RAD
      zz = z
      res = pj_transform(pj_wo,pj_spcs, 1_C_LONG, 0_C_INT, xx, yy, zz)
      if ( res == 0 ) then
        write(*,*) 'success',res,xx,yy
      else
        write(*,*) 'failure',res,xx,yy
        res = pj_transform(pj_ll,pj_spcs, 1_C_LONG, 0_C_INT, xx, yy, zz)
      endif
      x = xx
      y = yy
      z = zz
    end subroutine convert_to_spcs

end module convert_coords

integer function CK( iret )
  use netcdf
  integer,intent(in) :: iret
  if(iret.ne.NF90_NOERR) then
      print*, nf90_strerror(iret); stop
  endif
  CK=iret
end function CK

real function round( f, n )
! rounds f to n significant digits
  real,intent(in) :: f
  integer,intent(in) :: n
  real :: offset,precis,expo,bits,scal
  if (f == -9999. .or. f==0.0) then
    round = f
  else
    offset = 0.0
    if (abs(f) > 1e-16) then
      offset=int(floor(log10(abs(f))))
    endif
    precis = 10.**( offset-n )
    expo = log10(precis)
    if (expo < 0) then
      expo = int(floor(expo))
    else
      expo = int(ceiling(expo))
    endif
    bits = ceiling( log10( 10**(-expo) )/log10(2.0) )
    scal = 2.**bits
    round = nint(f*scal)/scal
  endif
end function round

program convSelfeBin2nc
!-------------------------------------------------------------------------------
!  use typeSizes
  use netcdf
  use convert_coords
  implicit real(4)(a-h,o-z),integer(i-n)
  include 'netcdf.inc'
  interface
    integer function CK( iret )
      integer,intent(in) :: iret
    end function CK
    real function round( f, n )
      real,intent(in) :: f
      integer,intent(in) :: n
    end function round
  end interface

  parameter(nbyte=4)
  character*30 file63
  character*12 it_char
  character*48 start_time,version,variable_nm,variable_dim
  character*48 data_format
  character*48 tbuf, c_time
  character*256 strtmp
  character*1 ctmp
  character*2 month, day
  character*4 year
  character(len= 4) :: a_4
  allocatable ztot(:),sigma(:),cs(:),outb(:,:,:),eta2(:),outeb(:,:,:),outsb(:,:,:)
  allocatable nm(:,:),nm2(:,:),js(:,:),xctr(:),yctr(:),dpe(:)
  allocatable x(:),y(:),dp(:),kbp00(:),iplg(:,:),ielg(:,:),kbp01(:,:)
  allocatable ylat(:),xlon(:),ycjlat(:),xcjlon(:),yctrlat(:),xctrlon(:)
  allocatable islg(:,:),kbs(:),kbe(:),xcj(:),ycj(:),dps(:),ic3(:,:),is(:,:),isidenode(:,:)
  allocatable nm_e(:,:),nm_s(:,:),eta2s(:),eta2e(:)
  ! command line args
  character*80   arg
  character*256  PATH
  character*256  OUTPATH
  integer first_stack, last_stack, nstacks
  character*4 data_loc ! node, side, cntr
  integer ihalf_lev ! 0 or 1

  !netcdf variables
  character(len=50) fname
  integer :: time_dims(1),ele_dims(2),x_dims(1),sigma_dims(1),var2d_dims(2),var3d_dims(3), &
            &data_start_2d(2),data_start_3d(3),data_count_2d(2),data_count_3d(3),z_dims(1), empty(0)
!-------------------------------------------------------------------------------
! Aquire user inputs
!-------------------------------------------------------------------------------
  call init_coord_sys()

  nargs = iargc()
  if ( nargs < 5 ) then
    write(*,*) 'Usage: '
    call getarg(0, arg)
    write(*,*) trim(arg)//' filename first_stack last_stack datadir outputdir [ndigits]'
    stop
  endif
  call getarg(1, file63)
  call getarg(2, arg)
  read(arg,*) ibgn
  call getarg(3, arg)
  read(arg,*) iend
  call getarg(4, PATH)
  call getarg(5, OUTPATH)
  if ( nargs == 6 ) then
    call getarg(6, arg)
    read(arg,*) ndigits
  else
    ndigits = -99
  endif

  write(*,*) 'Parsed arguments:'
  write(*,*) 'File:        ', file63
  write(*,*) 'Path:        ', trim(PATH)
  write(*,*) 'Output Dir:  ', trim(OUTPATH)
  write(*,*) 'First stack: ', ibgn
  write(*,*) 'Last stack:  ', iend
  if (ndigits > -99) then
    write(*,*) 'Rounding decimals: ', ndigits
  endif

  ! Compute ivs and itype; i23d_out for vis
  file63=adjustl(file63)
  lfile63=len_trim(file63)
  if(file63((lfile63-1):lfile63).eq.'61'.or.file63((lfile63-1):lfile63).eq.'62') then
    itype=2; i23d_out=2
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'63'.or.file63((lfile63-1):lfile63).eq.'64') then
    itype=3; i23d_out=3
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'65') then
    itype=4; i23d_out=2 !2D side
    data_loc='side'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'66') then
    itype=5; i23d_out=2 !2D element
    data_loc='cntr'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'67') then
    itype=6; i23d_out=3 !3D side and whole level
    data_loc='side'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'68') then
    itype=7; i23d_out=3 !3D side and half level
    data_loc='side'; ihalf_lev=1
  else if(file63((lfile63-1):lfile63).eq.'69') then
    itype=8; i23d_out=3 !3D elem. and whole level
    data_loc='cntr'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'70') then
    itype=9; i23d_out=3 !3D elem. and half level
    data_loc='cntr'; ihalf_lev=1
  else if(file63((lfile63-1):lfile63).eq.'71') then
    itype=10; i23d_out=2 !2D node
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'72') then
    itype=11; i23d_out=3 !3D node and whole level
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'73') then
    itype=12; i23d_out=3 !3D node and half level
    data_loc='node'; ihalf_lev=1
  else
    write(*,*) file63,file63((lfile63-1):lfile63),lfile63
    stop 'Unknown suffix'
  endif

!   Read header
  write(it_char,'(i12)') ibgn
  it_char=adjustl(it_char)

  open(63,file=trim(PATH)//trim(it_char)//'_'//file63,status='old',access='direct',recl=nbyte)
  irec=0
  do m=1,48/nbyte
    read(63,rec=irec+m) data_format(nbyte*(m-1)+1:nbyte*m)
  enddo
  if(data_format.ne.'DataFormat v5.0') then
    write(*,*) 'This code reads only v5.0:  ',data_format
    stop
  endif
  irec=irec+48/nbyte
  do m=1,48/nbyte
    read(63,rec=irec+m) version(nbyte*(m-1)+1:nbyte*m)
  enddo
  irec=irec+48/nbyte
  do m=1,48/nbyte
    read(63,rec=irec+m) start_time(nbyte*(m-1)+1:nbyte*m)
  enddo
  irec=irec+48/nbyte
  do m=1,48/nbyte
    read(63,rec=irec+m) variable_nm(nbyte*(m-1)+1:nbyte*m)
  enddo
  irec=irec+48/nbyte
  do m=1,48/nbyte
    read(63,rec=irec+m) variable_dim(nbyte*(m-1)+1:nbyte*m)
  enddo
  irec=irec+48/nbyte

  write(*,'(a48)')data_format
  write(*,'(a48)')version
  write(*,'(a48)')start_time
  write(*,'(a48)')variable_nm
  write(*,'(a48)')variable_dim

  read(63,rec=irec+1) nrec   ! # of output time steps (int)
  read(63,rec=irec+2) dtout  ! output time step (real)
  read(63,rec=irec+3) nspool ! skip (int)
  read(63,rec=irec+4) ivs    ! ivs (=1 or 2 for scalar or vector)
  read(63,rec=irec+5) i23d   ! i23d (=2 or 3 for 2D or 3D)
  irec=irec+5

!     Vertical grid
  read(63,rec=irec+1) nvrt    ! total # of vertical levels
  read(63,rec=irec+2) kz      ! # of Z-levels
  read(63,rec=irec+3) h0      ! dry threshold
  read(63,rec=irec+4) h_s     ! Z-sigma transition depth
  read(63,rec=irec+5) h_c     ! ~thickness of finely resolved surf layer
  read(63,rec=irec+6) theta_b ! sigma zoom param: 1: resolve surf+bottom, 0: only surf
  read(63,rec=irec+7) theta_f ! sigma zoom param: ->0: uniform sigma, >>1: coarser in middle
  irec=irec+7
  allocate(ztot(nvrt),sigma(nvrt),cs(nvrt),stat=istat)
  if(istat/=0) stop 'Failed to allocate (1)'

  do k=1,kz-1
    read(63,rec=irec+k) ztot(k) ! z-coordinates of each z-level
    ! deepest point ztot(1)
    ! actual bottom located at index kbp00 (or kbp or kbp2)
    ! kz : last index in z grid
  enddo
  do k=kz,nvrt
    kin=k-kz+1
    read(63,rec=irec+k) sigma(kin) ! sigma-coordinates of each S-level
    cs(kin)=(1-theta_b)*sinh(theta_f*sigma(kin))/sinh(theta_f)+ &
  &theta_b*(tanh(theta_f*(sigma(kin)+0.5))-tanh(theta_f*0.5))/2/tanh(theta_f*0.5)
  enddo
  irec=irec+nvrt

  irec_tmp=irec !save for non-standard outputs

  !     Horizontal grid
  if ( data_loc=='node' ) then
    read(63,rec=irec+1) np ! # of nodes
    read(63,rec=irec+2) ne ! # of elements
    irec=irec+2
    write(*,*) ' np, ne: ',np,ne

    allocate(x(np),y(np),dp(np),kbp00(np),nm2(ne,4),eta2(np),dpe(ne),kbe(ne),stat=istat)
    if(istat/=0) stop 'Failed to allocate 2D arrays'

    do m=1,np
      read(63,rec=irec+1) x(m)     ! x
      read(63,rec=irec+2) y(m)     ! y
      read(63,rec=irec+3) dp(m)    ! depth
      read(63,rec=irec+4) kbp00(m) ! initial bottom indices
      irec=irec+4
    enddo !m=1,np

    do m=1,ne
      read(63,rec=irec+1) i34      ! element type (currently must be 3)
      irec=irec+1
      do mm=1,i34
        read(63,rec=irec+1) nm2(m,mm) ! node # (connectivity table)
        irec=irec+1
      enddo !mm
    enddo !m
    irec0=irec
  else
    ! read standard grid from elev.61
    irec=irec_tmp
    open(64,file=trim(PATH)//trim(it_char)//'_elev.61',status='old',access='direct',recl=nbyte)
    read(64,rec=irec+1) np ! # of nodes
    read(64,rec=irec+2) ne ! # of elements
    irec=irec+2
    write(*,*) ' np, ne: ',np,ne

    allocate(x(np),y(np),dp(np),kbp00(np),nm2(ne,4),eta2(np),dpe(ne),kbe(ne),stat=istat)
    if(istat/=0) stop 'Failed to allocate 2D arrays'

    do m=1,np
      read(64,rec=irec+1) x(m)     ! x
      read(64,rec=irec+2) y(m)     ! y
      read(64,rec=irec+3) dp(m)    ! depth
      read(64,rec=irec+4) kbp00(m) ! initial bottom indices
      irec=irec+4
    enddo !m=1,np

    do m=1,ne
      read(64,rec=irec+1) i34      ! element type (currently must be 3)
      irec=irec+1
      do mm=1,i34
        read(64,rec=irec+1) nm2(m,mm) ! node # (connectivity table)
        irec=irec+1
      enddo !mm
    enddo !m
    close(64)
    ! get correct start index for time steps
    if ( data_loc=='cntr' ) then
      irec=irec_tmp
      read(63,rec=irec+1) ne2 ! # of elements
      read(63,rec=irec+2) ne_e ! # of triangles in element triangulation
      irec=irec+2
      write(*,*) ' ne, ne_e: ',ne2,ne_e
      if(ne2/=ne) then
        write(*,*)'Mismatch in elements:',ne2,ne
        stop
      endif
      ! start record for time steps
      irec0=irec_tmp + 2 + 4*ne2 + 4*ne_e
    else if ( data_loc=='side' ) then
      irec=irec_tmp
      read(63,rec=irec+1) ns2 ! # of sides
      read(63,rec=irec+2) ns_e ! # of triangles in side triangulation
      irec=irec+2
      write(*,*) ' ns, ns_e: ',ns2,ns_e
      ! start record for time steps
      irec0=irec_tmp + 2 + 4*ns2 + 4*ns_e
    endif
  endif
!  Done Reading header

  allocate(nm(ne,3), outb(np,nvrt,2), outeb(ne,nvrt,2),xctr(ne),yctr(ne), &
           & eta2e(ne),ylat(np),xlon(np),yctrlat(ne),xctrlon(ne),stat=istat)
  if(istat/=0) stop 'Allocation error: x,y'
  nm = nm2(:,1:3)

  
! Initialize outb for ivalid pts (below bottom etc)
  outb=-9999.
  outeb=-9999.

  ! compression parameters
  isuf = 1 ! SHUFFLE If non-zero, turn on the shuffle filter.
  icompr = 1 ! DEFLATE If non-zero, turn on the deflate filter.
  icompr_lev = 4 ! DEFLATE_LEVEL Must be between 0 and 9.
  ! chunking parameters (these have huge impact on I/O performance)
  nTimeChnk = 1   ! 1
  nNodeChnk = 512 ! 512 !large useful for write and slabs, bad for stations
  nVertChnk = 8  ! nvrt !large useful for stations, bad for slabs
  nCache = nVertChnk*nNodeChnk*nVertChnk*16

! Compute geometry
  call compute_nside(np,ne,nm,ns2)
  allocate(ic3(ne,3),js(ne,3),is(ns2,2),isidenode(ns2,2),xcj(ns2),ycj(ns2),ycjlat(ns2),xcjlon(ns2),stat=istat)
  if(istat/=0) stop 'Allocation error: side(0)'
  call selfe_geometry(np,ne,ns2,x,y,nm,ic3,js,is,isidenode,xcj,ycj)

  if(data_loc=='side' .and. ns2/=ns) then
    write(*,*)'Mismatch in sides:',ns2,ns
    stop
  endif

! Allocate side arrays
  allocate(dps(ns2),kbs(ns2),outsb(ns2,nvrt,2),eta2s(ns2),stat=istat)
  if(istat/=0) stop 'Allocation error: side'
  outsb=-9999.

! Compute side/element bottom index
  do i=1,ne
    kbe(i)=maxval(kbp00(nm(i,1:3)))
    dpe(i)=sum(dp(nm(i,1:3)))/3
    xctr(i)=sum(x(nm(i,1:3)))/3
    yctr(i)=sum(y(nm(i,1:3)))/3
  enddo !i
  do i=1,ns2
    kbs(i)=maxval(kbp00(isidenode(i,1:2)))
    dps(i)=sum(dp(isidenode(i,1:2)))/2
  enddo !i

!     Compute node coordinates in lat/lon
  do i=1,np
    xtmp = x(i)
    ytmp = y(i)
    ztmp = 0.0
    call convert_to_ll( xtmp,ytmp,ztmp )
    xlon(i) = xtmp
    ylat(i) = ytmp
  enddo
 if ( data_loc == 'side' ) then
    do i=1,ns2
      xtmp = xcj(i)
      ytmp = ycj(i)
      ztmp = 0.0
      call convert_to_ll( xtmp,ytmp,ztmp )
      xcjlon(i) = xtmp
      ycjlat(i) = ytmp
    enddo
  endif
 if ( data_loc == 'cntr' ) then
    do i=1,ne
      xtmp = xctr(i)
      ytmp = yctr(i)
      ztmp = 0.0
      call convert_to_ll( xtmp,ytmp,ztmp )
      xctrlon(i) = xtmp
      yctrlat(i) = ytmp
    enddo
  endif

!-------------------------------------------------------------------------------
! Time iteration -- select "node" data
!-------------------------------------------------------------------------------

! Loop over input files
  do iinput=ibgn,iend
    write(it_char,'(i12)')iinput
    it_char=adjustl(it_char)  !place blanks at end
    it_len=len_trim(it_char)  !length without trailing blanks

    !Write header to output files
    fname=trim(OUTPATH)//'/'//it_char(1:it_len)//'_'//file63(1:lfile63)//'.nc'
    write(*,*) 'generating file ',fname
    ir=CK(nf90_create(trim(fname), NF90_NETCDF4, ncid))
!     define dimensions
    ir=CK(nf90_def_dim(ncid, 'node',np, node_dim))
    ir=CK(nf90_def_dim(ncid, 'face',ne, nface_dim))
    if ( data_loc=='side' ) then
      ir=CK(nf90_def_dim(ncid, 'edge',ns2, nedge_dim))
    endif
    ir=CK(nf90_def_dim(ncid, 'max_face_nodes',3, nfacenode_dim))
    ir=CK(nf90_def_dim(ncid, 'layers',nvrt, layers_dim))
    ir=CK(nf90_def_dim(ncid, 'sigma',nvrt-kz+1, nsigma_dim))
    if(kz/=1) ir=CK(nf90_def_dim(ncid, 'nz',kz, nz_dim))
    ir=CK(nf90_def_dim(ncid, 'time', NF90_UNLIMITED, ntime_dim))
    ir=CK(nf90_def_dim(ncid, 'One', 1, n_one_dim))
    ir=CK(nf90_def_dim(ncid, 'Two', 2, n_two_dim))
    if ( data_loc=='node' ) then
      ndata_dim = node_dim
    else if ( data_loc=='side' ) then
      ndata_dim = nedge_dim
    else if ( data_loc=='cntr' ) then
      ndata_dim = nface_dim
    endif

!     define variables
    ir=CK(nf90_def_var(ncid,'Mesh',NF90_INT,varid=itopo_id))
    ir=CK(nf90_put_att(ncid,itopo_id,'cf_role','mesh_topology'))
    ir=CK(nf90_put_att(ncid,itopo_id,'long_name','Topology data of 2D unstructured mesh'))
    ir=CK(nf90_put_att(ncid,itopo_id,'topology_dimension',2))
    ir=CK(nf90_put_att(ncid,itopo_id,'node_coordinates','node_lon node_lat'))
    ir=CK(nf90_put_att(ncid,itopo_id,'face_node_connectivity','face_nodes'))
    ! attribute required if variables will be defined on edges
    if ( data_loc=='side' ) then
      ir=CK(nf90_put_att(ncid,itopo_id,'edge_node_connectivity','edge_nodes'))
      ! optional attribute (requires edge_node_connectivity)
!         ir=CK(nf90_put_att(ncid,itopo_id,'edge_coordinates','edge_x edge_y')
      ir=CK(nf90_put_att(ncid,itopo_id,'edge_coordinates','edge_lon edge_lat'))
    endif
    if ( data_loc == 'cntr' ) then
    ! optional attribute
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_coordinates','face_x face_y'))
      ir=CK(nf90_put_att(ncid,itopo_id,'face_coordinates','face_lon face_lat'))
    ! optional attribute (requires edge_node_connectivity)
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_edge_connectivity','Mesh_face_edges'))
    ! optional attribute
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_face_connectivity','Mesh_face_links'))
    endif

    ir=CK(nf90_def_var(ncid,'Lambert_Conformal',NF90_INT,varid=iproj_id))
    ir=CK(nf90_put_att(ncid,iproj_id,'grid_mapping_name','lambert_conformal_conic'))
    ir=CK(nf90_put_att(ncid,iproj_id,'standard_parallel',(/44.333333,46.000000/)))
    ir=CK(nf90_put_att(ncid,iproj_id,'longitude_of_central_meridian',-120.500000))
    ir=CK(nf90_put_att(ncid,iproj_id,'latitude_of_projection_origin',43.666667))
    ir=CK(nf90_put_att(ncid,iproj_id,'false_easting',609601.219202))
    ir=CK(nf90_put_att(ncid,iproj_id,'false_northing',0))
    ir=CK(nf90_put_att(ncid,iproj_id,'comment','ORSPCS-N NAD27'))

    ir=CK(nf90_def_var(ncid,'vert_coords',NF90_INT,varid=ivert_id))
    ir=CK(nf90_put_att(ncid,ivert_id,'standard_name','ocean_s_z_coordinate'))
    ir=CK(nf90_put_att(ncid,ivert_id,'long_name','S-Z Levels'))
    ir=CK(nf90_put_att(ncid,ivert_id,'units','m'))
    ir=CK(nf90_put_att(ncid,ivert_id,'positive','up'))
    strtmp = 'eta:elev depth:depth zlev:z s:sigma C:Cs h_c:h_c'
    ir=CK(nf90_put_att(ncid,ivert_id,'formula_terms',strtmp))

    ir=CK(nf90_def_var(ncid,'time',NF90_FLOAT,ntime_dim,itime_id))
    ! time:units="seconds since 2005-05-21 00:00:00 -8:00";
    c_time = start_time
    READ (c_time, "(a2,a1,a2,a1,a4)") month, ctmp, day, ctmp, year
!       write(*,*)year,'-',month,'-',day,' 00:00:00 -8:00'
    tbuf = 'seconds since ' // year // '-' // month // '-' // day // ' 00:00:00 -8:00'
    ir=CK(nf90_put_att(ncid,itime_id,'standard_name','time'))
    ir=CK(nf90_put_att(ncid,itime_id,'long_name','Time'))
    ir=CK(nf90_put_att(ncid,itime_id,'units','seconds'))
    ir=CK(nf90_put_att(ncid,itime_id,'base_date',trim(tbuf)))

    ir=CK(nf90_def_var(ncid,'face_nodes',NF90_INT,(/ nfacenode_dim, nface_dim /),iconn_id))
!       ir=CK(nf90_def_var_chunking(ncid,iconn_id, NF90_CHUNKED, (/3,nNodeChnk/))
    ir=CK(nf90_def_var_deflate(ncid,iconn_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,iconn_id,'cf_role','face_node_connectivity'))
    ir=CK(nf90_put_att(ncid,iconn_id,'long_name','Maps every face to its corner nodes.'))
    ir=CK(nf90_put_att(ncid,iconn_id,'start_index',1))
    ir=CK(nf90_def_var_fill(ncid, iconn_id, 0, 9999999))

    if ( data_loc=='side' ) then
      ir=CK(nf90_def_var(ncid,'edge_nodes',NF90_INT,(/ n_two_dim, nedge_dim /),ieconn_id))
!         ir=CK(nf90_def_var_chunking(ncid,ieconn_id, NF90_CHUNKED, (/2,nNodeChnk/))
      ir=CK(nf90_def_var_deflate(ncid,ieconn_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ieconn_id,'cf_role','edge_node_connectivity'))
      ir=CK(nf90_put_att(ncid,ieconn_id,'long_name','Maps every edge to the two end nodes'))
      ir=CK(nf90_put_att(ncid,ieconn_id,'start_index',1))
      ir=CK(nf90_def_var_fill(ncid, ieconn_id, 0, 9999999))

      ir=CK(nf90_def_var(ncid,'edge_x',NF90_DOUBLE,nedge_dim,iex_id))
      ir=CK(nf90_def_var_chunking(ncid,iex_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,iex_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iex_id,'standard_name','projection_x_coordinate'))
      ir=CK(nf90_put_att(ncid,iex_id,'long_name','x-coordinates of edge midpoints'))
      ir=CK(nf90_put_att(ncid,iex_id,'units','m'))
      ir=CK(nf90_put_att(ncid,iex_id,'axis','X'))

      ir=CK(nf90_def_var(ncid,'edge_y',NF90_DOUBLE,nedge_dim,iey_id))
      ir=CK(nf90_def_var_chunking(ncid,iey_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,iey_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iey_id,'standard_name','projection_y_coordinate'))
      ir=CK(nf90_put_att(ncid,iey_id,'long_name','y-coordinates of edge midpoints'))
      ir=CK(nf90_put_att(ncid,iey_id,'units','m'))
      ir=CK(nf90_put_att(ncid,iey_id,'axis','Y'))

      ir=CK(nf90_def_var(ncid,'edge_lon',NF90_DOUBLE,nedge_dim,ielon_id))
      ir=CK(nf90_def_var_chunking(ncid,ielon_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,ielon_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ielon_id,'standard_name','longitude'))
      ir=CK(nf90_put_att(ncid,ielon_id,'long_name','Longitude of edge midpoints'))
      ir=CK(nf90_put_att(ncid,ielon_id,'units','degrees_east'))

      ir=CK(nf90_def_var(ncid,'edge_lat',NF90_DOUBLE,nedge_dim,ielat_id))
      ir=CK(nf90_def_var_chunking(ncid,ielat_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,ielat_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ielat_id,'standard_name','latitude'))
      ir=CK(nf90_put_att(ncid,ielat_id,'long_name','Latitude of edge midpoints'))
      ir=CK(nf90_put_att(ncid,ielat_id,'units','degrees_north'))

    endif

    if ( data_loc=='cntr' ) then
      ir=CK(nf90_def_var(ncid,'face_x',NF90_DOUBLE,nface_dim,ifx_id))
      ir=CK(nf90_def_var_chunking(ncid,ifx_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,ifx_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ifx_id,'standard_name','projection_x_coordinate'))
      ir=CK(nf90_put_att(ncid,ifx_id,'long_name','x-coordinates of face centers'))
      ir=CK(nf90_put_att(ncid,ifx_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ifx_id,'axis','X'))

      ir=CK(nf90_def_var(ncid,'face_y',NF90_DOUBLE,nface_dim,ify_id))
      ir=CK(nf90_def_var_chunking(ncid,ify_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,ify_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ify_id,'standard_name','projection_y_coordinate'))
      ir=CK(nf90_put_att(ncid,ify_id,'long_name','y-coordinates of face centers'))
      ir=CK(nf90_put_att(ncid,ify_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ify_id,'axis','Y'))

      ir=CK(nf90_def_var(ncid,'face_lon',NF90_DOUBLE,nface_dim,iflon_id))
      ir=CK(nf90_def_var_chunking(ncid,iflon_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,iflon_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iflon_id,'standard_name','longitude'))
      ir=CK(nf90_put_att(ncid,iflon_id,'long_name','Longitude of face centers'))
      ir=CK(nf90_put_att(ncid,iflon_id,'units','degrees_east'))

      ir=CK(nf90_def_var(ncid,'face_lat',NF90_DOUBLE,nface_dim,iflat_id))
      ir=CK(nf90_def_var_chunking(ncid,iflat_id, NF90_CHUNKED, (/nNodeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,iflat_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iflat_id,'standard_name','latitude'))
      ir=CK(nf90_put_att(ncid,iflat_id,'long_name','Latitude of face centers'))
      ir=CK(nf90_put_att(ncid,iflat_id,'units','degrees_north'))
    endif

    ir=CK(nf90_def_var(ncid,'node_x',NF90_DOUBLE,node_dim,ix_id))
    ir=CK(nf90_def_var_chunking(ncid,ix_id, NF90_CHUNKED, (/nNodeChnk/)))
    ir=CK(nf90_def_var_deflate(ncid,ix_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,ix_id,'standard_name','projection_x_coordinate'))
    ir=CK(nf90_put_att(ncid,ix_id,'long_name','x-coordinates of 2D mesh nodes'))
    ir=CK(nf90_put_att(ncid,ix_id,'units','m'))
    ir=CK(nf90_put_att(ncid,ix_id,'axis','X'))

    ir=CK(nf90_def_var(ncid,'node_y',NF90_DOUBLE,node_dim,iy_id))
    ir=CK(nf90_def_var_chunking(ncid,iy_id, NF90_CHUNKED, (/nNodeChnk/)))
    ir=CK(nf90_def_var_deflate(ncid,iy_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,iy_id,'standard_name','projection_y_coordinate'))
    ir=CK(nf90_put_att(ncid,iy_id,'long_name','y-coordinates of 2D mesh nodes'))
    ir=CK(nf90_put_att(ncid,iy_id,'units','m'))
    ir=CK(nf90_put_att(ncid,iy_id,'axis','Y'))

    ir=CK(nf90_def_var(ncid,'node_lon',NF90_DOUBLE,node_dim,ilon_id))
    ir=CK(nf90_def_var_chunking(ncid,ilon_id, NF90_CHUNKED, (/nNodeChnk/)))
    ir=CK(nf90_def_var_deflate(ncid,ilon_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,ilon_id,'standard_name','longitude'))
    ir=CK(nf90_put_att(ncid,ilon_id,'long_name','Longitude of 2D mesh nodes'))
    ir=CK(nf90_put_att(ncid,ilon_id,'units','degrees_east'))

    ir=CK(nf90_def_var(ncid,'node_lat',NF90_DOUBLE,node_dim,ilat_id))
    ir=CK(nf90_def_var_chunking(ncid,ilat_id, NF90_CHUNKED, (/nNodeChnk/)))
    ir=CK(nf90_def_var_deflate(ncid,ilat_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,ilat_id,'standard_name','latitude'))
    ir=CK(nf90_put_att(ncid,ilat_id,'long_name','Latitude of 2D mesh nodes'))
    ir=CK(nf90_put_att(ncid,ilat_id,'units','degrees_north'))

    ir=CK(nf90_def_var(ncid,'sigma',NF90_DOUBLE,nsigma_dim,isigma_id))
    ir=CK(nf90_put_att(ncid,isigma_id,'long_name','S coordinates at whole levels'))
    ir=CK(nf90_put_att(ncid,isigma_id,'units','non-dimensional'))
    ir=CK(nf90_put_att(ncid,isigma_id,'positive','up'))

    if(kz/=1) then
      ir=CK(nf90_def_var(ncid,'z',NF90_FLOAT,nz_dim,iz_id))
      ir=CK(nf90_put_att(ncid,iz_id,'long_name','Z coordinates at whole levels'))
      ir=CK(nf90_put_att(ncid,iz_id,'units','m'))
      ir=CK(nf90_put_att(ncid,iz_id,'positive','up'))
    endif

    ir=CK(nf90_def_var(ncid,'depth',NF90_FLOAT,ndata_dim,idepth_id))
    if (data_loc=='node') then
      ir=CK(nf90_put_att(ncid,idepth_id,'location','node'))
      ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','node_lon node_lat'))
    else if (data_loc=='side') then
      ir=CK(nf90_put_att(ncid,idepth_id,'location','edge'))
      ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','edge_lon edge_lat'))
    else if (data_loc=='cntr') then
      ir=CK(nf90_put_att(ncid,idepth_id,'location','face'))
      ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','face_lon face_lat'))
    endif
    ir=CK(nf90_def_var_chunking(ncid,idepth_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
    ir=CK(nf90_def_var_deflate(ncid,idepth_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,idepth_id,'standard_name','sea_floor_depth_below_sea_level'))
    ir=CK(nf90_put_att(ncid,idepth_id,'long_name','Bathymetry'))
    ir=CK(nf90_put_att(ncid,idepth_id,'units','m'))
    ir=CK(nf90_put_att(ncid,idepth_id,'positive','down'))
    ir=CK(nf90_put_att(ncid,idepth_id,'missing_value','-9999.'))
    ir=CK(nf90_put_att(ncid,idepth_id,'mesh','Mesh'))

    ir=CK(nf90_def_var(ncid,'elev',NF90_FLOAT,(/ ndata_dim, ntime_dim /),ielev_id))
    if (data_loc=='node') then
      ir=CK(nf90_put_att(ncid,ielev_id,'location','node'))
      ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','node_lon node_lat'))
    else if (data_loc=='side') then
      ir=CK(nf90_put_att(ncid,ielev_id,'location','edge'))
      ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','edge_lon edge_lat'))
    else if (data_loc=='cntr') then
      ir=CK(nf90_put_att(ncid,ielev_id,'location','face'))
      ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','face_lon face_lat'))
    endif
      ir=CK(nf90_def_var_chunking(ncid,ielev_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
      ir=CK(nf90_def_var_deflate(ncid,ielev_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ielev_id,'standard_name','sea_surface_height_above_sea_level'))
      ir=CK(nf90_put_att(ncid,ielev_id,'long_name','Sea surface elevation'))
      ir=CK(nf90_put_att(ncid,ielev_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ielev_id,'positive','up'))
      ir=CK(nf90_put_att(ncid,ielev_id,'missing_value','-9999.'))
      ir=CK(nf90_put_att(ncid,ielev_id,'mesh','Mesh'))

    ir=CK(nf90_def_var(ncid,'Cs',NF90_FLOAT,nsigma_dim,ics_id))
    ir=CK(nf90_put_att(ncid,ics_id,'long_name','Function C(s) at whole levels'))
    ir=CK(nf90_put_att(ncid,ics_id,'units','non-dimensional'))
    ir=CK(nf90_put_att(ncid,ics_id,'positive','up'))

    ir=CK(nf90_def_var(ncid,'h_c',NF90_FLOAT,n_one_dim,ihc_id))
    ir=CK(nf90_put_att(ncid,ihc_id,'long_name','S-Z coordinate surface layer depth'))
    ir=CK(nf90_put_att(ncid,ihc_id,'units','m'))
    ir=CK(nf90_put_att(ncid,ihc_id,'positive','up'))

    ir=CK(nf90_def_var(ncid,'k_bottom',NF90_INT,ndata_dim,ikb_id))
    ir=CK(nf90_def_var_deflate(ncid,ikb_id,isuf,icompr,icompr_lev))
    ir=CK(nf90_put_att(ncid,ikb_id,'standard_name','model_level_number_at_sea_floor'))
    ir=CK(nf90_put_att(ncid,ikb_id,'long_name','bottom level index'))
    ir=CK(nf90_put_att(ncid,ikb_id,'units','1'))
    ir=CK(nf90_put_att(ncid,ikb_id,'positive','up'))

    if(i23d_out==2 ) then
      if ( file63(1:lfile63-3) /= 'elev' ) then
        write(*,*) 'this is a 2D var'
        var2d_dims(1)=ndata_dim
        var2d_dims(2)=ntime_dim
        if ( ivs == 1 ) then
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3),NF90_FLOAT,var2d_dims,ivar_id))
          ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
          ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
        else
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_x',NF90_FLOAT,var2d_dims,ivar_id))
          ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
          ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_y',NF90_FLOAT,var2d_dims,ivar2_id))
          ir=CK(nf90_def_var_chunking(ncid,ivar2_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
          ir=CK(nf90_def_var_deflate(ncid,ivar2_id,isuf,icompr,icompr_lev))
        endif
        ! set standard_name and units
        if ( file63(1:lfile63-3) == 'pres' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','air_pressure_at_sea_level'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','Pa'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air pressure at sea surface'))
        else if ( file63(1:lfile63-3) == 'airt' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_temperature'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','degree_Celsius'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air temperature at sea surface'))
        else if ( file63(1:lfile63-3) == 'shum' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_specific_humidity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air humidity at sea surface'))
        else if ( file63(1:lfile63-3) == 'srad' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downwelling_shortwave_flux_in_air'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Solar radiation'))
        else if ( file63(1:lfile63-3) == 'flsu' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upward_sensible_heat_flux'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Sensible heat flux at sea surface'))
        else if ( file63(1:lfile63-3) == 'fllu' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upward_latent_heat_flux'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Latent heat flux at sea surface'))
        else if ( file63(1:lfile63-3) == 'radu' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upwelling_longwave_flux_in_air'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Upwelling infrared (longwave) radiative flux at sea surface'))
        else if ( file63(1:lfile63-3) == 'radd' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downwelling_longwave_flux_in_air'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Downwelling infrared (longwave) radiative flux at sea surface'))
        else if ( file63(1:lfile63-3) == 'flux' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','downward_heat_flux_in_air'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Net downward heat flux at sea surface (except for solar)'))
        else if ( file63(1:lfile63-3) == 'evap' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','water_evaporation_flux'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Evaporation rate'))
        else if ( file63(1:lfile63-3) == 'prcp' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','precipitation_flux'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Precipitation rate'))
        else if ( file63(1:lfile63-3) == 'dahv' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_depth_averaged_velocity')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Depth averaged x velocty'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_depth_averaged_velocity')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Depth averaged y velocty'))
        else if ( file63(1:lfile63-3) == 'dihv' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_depth_integrated_velocity')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Depth integrated x velocty'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_depth_integtated_velocity')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar2_id,'units','m2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Depth integrated y velocty'))
        else if ( file63(1:lfile63-3) == 'wind' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','x_wind'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Wind x velocity'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','y_wind'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Wind y velocity'))
        else if ( file63(1:lfile63-3) == 'wist' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downward_x_stress'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Wind induced momentum x flux'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','surface_downward_y_stress'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'units','m2 s-2'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Wind induced momentum y flux'))
        else
          write(*,*) 'Unknown 2D variable ', file63(1:lfile63-3)
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name',file63(1:lfile63-3)))
        endif
        ir=CK(nf90_put_att(ncid,ivar_id,'missing_value','-9999.'))
        ir=CK(nf90_put_att(ncid,ivar_id,'mesh','Mesh'))
        if (data_loc=='node') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','node'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','node_lon node_lat'))
        else if (data_loc=='side') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','edge'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','edge_lon edge_lat'))
        else if (data_loc=='cntr') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','face'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','face_lon face_lat'))
        endif
        if ( ivs == 2 ) then
          ir=CK(nf90_put_att(ncid,ivar2_id,'missing_value','-9999.'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'mesh','Mesh'))
          if (data_loc=='node') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','node'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','node_lon node_lat'))
          else if (data_loc=='side') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','edge'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','edge_lon edge_lat'))
          else if (data_loc=='cntr') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','face'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','face_lon face_lat'))
          endif
        endif
      endif
    else !3D
      write(*,*) 'this is a 3D var'
      var3d_dims(1)=ndata_dim
      var3d_dims(2)=layers_dim; var3d_dims(3)=ntime_dim
      if ( ivs == 1 ) then
        ir=CK(nf90_def_var(ncid,file63(1:lfile63-3),NF90_FLOAT,var3d_dims,ivar_id))
        ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
        ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
      else
        ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_x',NF90_FLOAT,var3d_dims,ivar_id))
        ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
        ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_y',NF90_FLOAT,var3d_dims,ivar2_id))
        ir=CK(nf90_def_var_chunking(ncid,ivar2_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
        ir=CK(nf90_def_var_deflate(ncid,ivar2_id,isuf,icompr,icompr_lev))
      endif
      if ( file63(1:lfile63-3) == 'salt' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_salinity'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','1e-3'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Salinity'))
      else if ( file63(1:lfile63-3) == 'temp' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_temperature'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','degree_Celsius'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Temperature'))
      else if ( file63(1:lfile63-3) == 'vert' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','upward_sea_water_velocity'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical velocity component'))
      else if ( file63(1:lfile63-3) == 'vdff' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_momentum_diffusivity'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy viscosty'))
      else if ( file63(1:lfile63-3) == 'tdff' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_tracer_diffusivity'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy diffusivity of tracers'))
      else if ( file63(1:lfile63-3) == 'kine' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_eddy_kinetic_energy')) ! No standard
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-2'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy kinetic energy'))
      else if ( file63(1:lfile63-3) == 'mixl' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_eddy_mixing_length')) ! No standard
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical turbulent mixing lenght'))

      else if ( file63(1:lfile63-3) == 'conc' ) then ! TODO check
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_density'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-3'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Water density'))
      else if ( file63(1:lfile63-3) == 'zcor' ) then ! TODO check
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','model_z_coordinate')) ! No standard
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical coordinates'))
      else if ( file63(1:lfile63-3) == 'qnon' ) then ! TODO check
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','normalized_non_hydrostatic_pressure')) ! No standard
        ir=CK(nf90_put_att(ncid,ivar_id,'units','Pa'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','normalized non-hydrostatic pressure'))

      else if ( file63(1:lfile63-3) == 'hvel' ) then
        ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_velocity'))
        ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name','horizontal y velocity component'))
        ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_velocity'))
        ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
        ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','horizontal y velocity component'))
      else
        write(*,*) 'Unknown 3D variable ', file63(1:lfile63-3)
        ir=CK(nf90_put_att(ncid,ivar_id,'long_name',file63(1:lfile63-3)))
      endif
      ir=CK(nf90_put_att(ncid,ivar_id,'missing_value','-9999.'))
      ir=CK(nf90_put_att(ncid,ivar_id,'mesh','Mesh'))
      if (data_loc=='node') then
        ir=CK(nf90_put_att(ncid,ivar_id,'location','node'))
        ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','node_lon node_lat'))
      else if (data_loc=='side') then
        ir=CK(nf90_put_att(ncid,ivar_id,'location','edge'))
        ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','edge_lon edge_lat'))
      else if (data_loc=='cntr') then
        ir=CK(nf90_put_att(ncid,ivar_id,'location','face'))
        ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','face_lon face_lat'))
      endif
      if ( ivs == 2 ) then
        ir=CK(nf90_put_att(ncid,ivar2_id,'missing_value','-9999.'))
        ir=CK(nf90_put_att(ncid,ivar2_id,'mesh','Mesh'))
        if (data_loc=='node') then
          ir=CK(nf90_put_att(ncid,ivar2_id,'location','node'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','node_lon node_lat'))
        else if (data_loc=='side') then
          ir=CK(nf90_put_att(ncid,ivar2_id,'location','edge'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','edge_lon edge_lat'))
        else if (data_loc=='cntr') then
          ir=CK(nf90_put_att(ncid,ivar2_id,'location','face'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','face_lon face_lat'))
        endif
      endif
    endif

    ir=CK(nf90_put_att(ncid, NF90_GLOBAL, 'Conventions', 'UGRID CF'))
!     leave define mode
    ir=CK(nf90_enddef(ncid))

!     Write mode (header part only)
    ir=CK(nf90_put_var(ncid,iconn_id,transpose(nm),(/1,1/),(/3,ne/)))
    if ( data_loc=='side' ) then
      ir=CK(nf90_put_var(ncid,ieconn_id,transpose(isidenode),(/1,1/),(/2,ns2/)))
    endif
    ir=CK(nf90_put_var(ncid,ix_id,x,(/1/),(/np/)))
    ir=CK(nf90_put_var(ncid,iy_id,y,(/1/),(/np/)))
    ir=CK(nf90_put_var(ncid,ilon_id,xlon,(/1/),(/np/)))
    ir=CK(nf90_put_var(ncid,ilat_id,ylat,(/1/),(/np/)))
    if ( data_loc == 'side' ) then
      ir=CK(nf90_put_var(ncid,iex_id,xcj,(/1/),(/ns2/)))
      ir=CK(nf90_put_var(ncid,iey_id,ycj,(/1/),(/ns2/)))
      ir=CK(nf90_put_var(ncid,ielon_id,xcjlon,(/1/),(/ns2/)))
      ir=CK(nf90_put_var(ncid,ielat_id,ycjlat,(/1/),(/ns2/)))
    endif
    if ( data_loc == 'cntr' ) then
      ir=CK(nf90_put_var(ncid,ifx_id,xctr,(/1/),(/ne/)))
      ir=CK(nf90_put_var(ncid,ify_id,yctr,(/1/),(/ne/)))
      ir=CK(nf90_put_var(ncid,iflon_id,xctrlon,(/1/),(/ne/)))
      ir=CK(nf90_put_var(ncid,iflat_id,yctrlat,(/1/),(/ne/)))
    endif
    if (data_loc=='node') then
      ir=CK(nf90_put_var(ncid,idepth_id,dp,(/1/),(/np/)))
      ir=CK(nf90_put_var(ncid,ikb_id,kbp00,(/1/),(/np/)))
    else if (data_loc=='side') then ! TODO change dp
      ir=CK(nf90_put_var(ncid,idepth_id,dps,(/1/),(/ns2/)))
      ir=CK(nf90_put_var(ncid,ikb_id,kbs,(/1/),(/ns2/)))
    else if (data_loc=='cntr') then
      ir=CK(nf90_put_var(ncid,idepth_id,dpe,(/1/),(/ne/)))
      ir=CK(nf90_put_var(ncid,ikb_id,kbe,(/1/),(/ne/)))
    endif
    ir=CK(nf90_put_var(ncid,isigma_id,sigma,(/1/),(/nvrt-kz+1/)))
    ir=CK(nf90_put_var(ncid,ics_id,cs,(/1/),(/nvrt-kz+1/)))
    ztot(kz)=h_s ! add last z level
    if(kz/=1) ir=CK(nf90_put_var(ncid,iz_id,ztot,(/1/),(/kz/)))
    ir=CK(nf90_put_var(ncid,ihc_id,h_c))

    !print*, 'Last element:',nm(ne,1:3)
    !end output header

    ! Loop over output spools in file
    irec=irec0
    do ispool=1,nrec

      ! Read all data to outb, outeb, or outsb, and eta2
      open(63,file=trim(PATH)//'/'//it_char(1:it_len)//'_'//file63,access='direct',status='old',recl=nbyte)
      read(63,rec=irec+1) time
      read(63,rec=irec+2) it
      irec=irec+2
      if(i23d_out==2 .and. data_loc=='node') then
        do i=1,np
          read(63,rec=irec+1) eta2(i)
          irec=irec+1
        enddo !i
        do i=1,np
          do m=1,ivs
            read(63,rec=irec+1) outb(i,1,m)
            irec=irec+1
          enddo !m
        enddo !i

      else if(i23d_out==3 .and. data_loc=='node') then !3D
        do i=1,np
          read(63,rec=irec+1) eta2(i)
          irec=irec+1
        enddo !i
        do i=1,np
          do k=max0(1,kbp00(i)),nvrt
            do m=1,ivs
              read(63,rec=irec+1) outb(i,k,m)
              irec=irec+1
            enddo !m
          enddo
        enddo !i
      else if(i23d_out==2 .and. data_loc=='side') then !2D side
        do i=1,ns
          read(63,rec=irec+1) eta2s(i)
          irec=irec+1
        enddo !i
        do i=1,ns
          do m=1,ivs
            read(63,rec=irec+1) outsb(i,1,m)
            irec=irec+1
          enddo !m
        enddo !i

      else if(i23d_out==2 .and. data_loc=='cntr') then !2D elem
        do i=1,ne
          read(63,rec=irec+1) eta2e(i)
          irec=irec+1
        enddo !i
        do i=1,ne
          do m=1,ivs
            read(63,rec=irec+1) outeb(i,1,m)
            irec=irec+1
          enddo !m
        enddo !i

      else if(i23d_out==3 .and. data_loc=='side') then !3D side and whole/half level
        do i=1,ns
          read(63,rec=irec+1) eta2s(i)
          irec=irec+1
        enddo !i
        do i=1,ns
          do k=max0(1,kbs(i)),nvrt
            do m=1,ivs
              read(63,rec=irec+1) outsb(i,k,m)
              irec=irec+1
            enddo !m
          enddo
        enddo !i

      else if(i23d_out==3 .and. data_loc=='cntr') then !3D element at whole/half levels
        do i=1,ne
          read(63,rec=irec+1) eta2e(i)
          irec=irec+1
        enddo !i
        do i=1,ne
          do k=max0(1,kbe(i)),nvrt
            do m=1,ivs
              read(63,rec=irec+1) outeb(i,k,m)
              irec=irec+1
            enddo !m
          enddo
        enddo !i
      endif
      ! Close input file
      close(63)

      ! reduce data precision
      if (ndigits > -99) then
        if (data_loc=='node') then
          if ( i23d_out==2 ) then
            do i=1,np
              do m=1,ivs
                outb(i,1,m) = round(outb(i,1,m),ndigits)
              enddo
            enddo
          else ! 3D
            do i=1,np
              do k=max0(1,kbp00(i)),nvrt
                do m=1,ivs
                  outb(i,k,m) = round(outb(i,k,m),ndigits)
                enddo
              enddo
            enddo
          endif ! i23d_out
        else if (data_loc=='side') then
          if ( i23d_out==2 ) then
            do i=1,ns
              do m=1,ivs
                outsb(i,1,m) = round(outsb(i,1,m),ndigits)
              enddo
            enddo
          else ! 3D
            do i=1,ns
              do k=max0(1,kbs(i)),nvrt
                do m=1,ivs
                  outsb(i,k,m) = round(outsb(i,k,m),ndigits)
                enddo
              enddo
            enddo
          endif ! i23d_out
        else if (data_loc=='cntr') then
          if ( i23d_out==2 ) then
            do i=1,ne
              do m=1,ivs
                outeb(i,1,m) = round(outeb(i,1,m),ndigits)
              enddo
            enddo
          else ! 3D
            do i=1,ne
              do k=max0(1,kbe(i)),nvrt
                do m=1,ivs
                  outeb(i,k,m) = round(outeb(i,k,m),ndigits)
                enddo
              enddo
            enddo
          endif ! i23d_out
        endif ! data_loc
      endif ! ndigits

      ! write to netcdf file
      ! write time
      ir=CK(nf90_put_var(ncid,itime_id,time,(/ ispool /)))
      ! write elev
      data_start_2d(1)=1; data_start_2d(2)=ispool
      data_count_2d(2)=1
      if (data_loc=='node') then
        data_count_2d(1)=np
        ir=CK(nf90_put_var(ncid,ielev_id,eta2,data_start_2d,data_count_2d))
        if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
          ir=CK(nf90_put_var(ncid,ivar_id,outb(:,1,1),data_start_2d,data_count_2d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outb(:,1,2),data_start_2d,data_count_2d))
          endif
        endif
      else if (data_loc=='side') then
        data_count_2d(1)=ns2
        ir=CK(nf90_put_var(ncid,ielev_id,eta2s,data_start_2d,data_count_2d))
        if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
          ir=CK(nf90_put_var(ncid,ivar_id,outsb(:,1,1),data_start_2d,data_count_2d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outsb(:,1,2),data_start_2d,data_count_2d))
          endif
        endif
      else if (data_loc=='cntr') then
        data_count_2d(1)=ne
        ir=CK(nf90_put_var(ncid,ielev_id,eta2e,data_start_2d,data_count_2d))
        if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
          ir=CK(nf90_put_var(ncid,ivar_id,outeb(:,1,1),data_start_2d,data_count_2d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outeb(:,1,2),data_start_2d,data_count_2d))
          endif
        endif
      endif
      if ( i23d_out==3 ) then !3D
        data_start_3d(1:2)=1; data_start_3d(3)=ispool
        data_count_3d(2)=nvrt; data_count_3d(3)=1
        if (data_loc=='node') then
          data_count_3d(1)=np
          ir=CK(nf90_put_var(ncid,ivar_id,outb(:,:,1),data_start_3d,data_count_3d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outb(:,:,2),data_start_3d,data_count_3d))
          endif
        else if (data_loc=='side') then
          data_count_3d(1)=ns2
          ir=CK(nf90_put_var(ncid,ivar_id,outsb(:,:,1),data_start_3d,data_count_3d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outsb(:,:,2),data_start_3d,data_count_3d))
          endif
        else if (data_loc=='cntr') then
          data_count_3d(1)=ne
          ir=CK(nf90_put_var(ncid,ivar_id,outeb(:,:,1),data_start_3d,data_count_3d))
          if( ivs==2 ) then
            ir=CK(nf90_put_var(ncid,ivar2_id,outeb(:,:,2),data_start_3d,data_count_3d))
          endif
        endif
      endif !i23d_out
      print*, 'done record # ',ispool

    enddo !ispool=1,nrec
    iret = nf90_close(ncid)
  enddo !iinput


  stop
end program convSelfeBin2nc
