""" Combines sediment files into a 'turbidity' file """
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
from optparse import OptionParser

import netCDF4

try:
	from autocombine import makeTurbidity
except:
	import sys
	sys.path.append('/home/workspace/users/lopezj/src/selfe_junk/svn/trunk/src/utility/Combining_Scripts/')
	from autocombine import makeTurbidity

#-------------------------------------------------------------------------------
# Functions 
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Main 
#-------------------------------------------------------------------------------
def main(firstStack, lastStack, dir, nTracers):
  """ Make turbidity files for each stack """
  for stack in range(firstStack, lastStack+1):
    print 'Making turbidity file for stack %d' % stack
    makeTurbidity(dir, stack, nTracers)

def parse():
  """ Parse CLI """
  usage = ('Usage: %prog firstStack lastStack dataDir nTracers\n')
  parser = OptionParser(usage=usage)
  (options, args) = parser.parse_args()

  if len(args) != 4 :
    parser.print_help()
    parser.error('Could not parse firstStack, lastStack')
  firstStack = int(args[0])
  lastStack = int(args[1])
  dir = args[2]
  nTracers = int(args[3])

  print 'Parsed options:'
  print ' - stacks ', firstStack, lastStack
  print ' - dir ', dir
  print ' - nTracers ', nTracers

  main(firstStack, lastStack, dir, nTracers)

if __name__ == '__main__':
  parse()
