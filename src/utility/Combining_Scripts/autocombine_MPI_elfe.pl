#! /usr/bin/perl -w

# This script can be run before, during or after a parallel SELFE run (e.g., using mpiexec or qsub) 
# It is useful when you don't know the strucutre of the cluster.
# It runs as a daemon and will combine outputs along the way.
# Run on the run dir (i.e., one level above outputs/) on any system (but
# make sure the combine code below is compatible).
# Assume that *_elev.61 is always output, and that 
# the code will output (end_stack+1)_00* (otherwise the script will hang).
# For side-/center-based outputs, the combine code needs sidecenters.gr3/centers.gr3.

#use Cwd;

if(@ARGV != 5) 
{ 
  print "$0 <start # of stacks> <end # of stacks> <netcdf flag - 0 no, 1 yes> <input dir> <output dir>\n";
  exit(1);
}
print "$0 @ARGV\n";
$start_stack=$ARGV[0]; 
$end_stack=$ARGV[1]; 
$netcdf=$ARGV[2];
$indir=$ARGV[3];
$outdir=$ARGV[4];

#Get nproc and ntracers
if(!-e $indir) {die "No model data $indir dir!";}
if(!-e $outdir) {die "No $outdir dir!";}
open(FL,"<$indir/local_to_global_0000"); @all=<FL>; close(FL);
($_,$_,$_,$nproc,$ntr)=split(" ",$all[0]);

$code="/usr/local/selfe/bin/combine_output7";
@vars=('elev.61','hvel.64','pres.61','airt.61','shum.61','srad.61',
       'flsu.61','fllu.61','radu.61','radd.61','flux.61','evap.61',
       'prcp.61','wind.62','wist.62','dahv.62','vert.63','temp.63',
       'salt.63','conc.63','tdff.63','vdff.63','kine.63','mixl.63',
       'zcor.63','qnon.63','totN.63','hvel.67','vert.69','temp.70',
       'salt.70','dihv.71');

for($i=1;$i<=$ntr;$i++)
{
  $name="trcr_$i.63";
  @vars=(@vars,$name);
} #for

for($i=1;$i<=25;$i++)
{
  if($i<=23)
  {$name="wwm_$i.61";}
  else
  {$name="wwm_$i.62";}
  @vars=(@vars,$name);
} #for

for($next_stack=$start_stack+1; $next_stack<=$end_stack+1; $next_stack++) {
  while(!-e "$indir/$next_stack\_0000_elev.61") {sleep 300;} #sleep 5 min.
#  sleep 180; #wait a little longer to make sure outputs are written
  $current_stack=$next_stack-1;
  foreach $var (@vars) {
    if(!-e "$indir/$current_stack\_0000_$var") {next;}
    for($i=0;$i<$nproc;$i++) {
      $id=sprintf("%04d",$i);
      while(!-e "$indir/$next_stack\_$id\_$var") {sleep 180;} #sleep 3 min.
    } #for

    # combine_output7 filename first_stack last_stack inetcdf datadir outputdir [ndigits]
    print "$code $var $current_stack $current_stack $netcdf $indir $outdir\n";
    system "$code $var $current_stack $current_stack $netcdf $indir $outdir";
    print "done combining $var in stack $current_stack...\n";
  } #foreach
} #for

#if(-e "outputs/$end_stack\_hvel.64") {system "cd outputs/; echo *_[0-9]???_*.6? | xargs rm -f";}
