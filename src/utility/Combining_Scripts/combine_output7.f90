!===============================================================================
! Read in binary outputs (rank-specific) from parallel code and combine them into
! one global output in v5.0 format or netcdf format. Works for partial outputs.
! Global-local mappings are read in from separate files.
! Run this program inside the directory outputs/, where some of the input files below
! can be found.

! Usage:
! combine_output7 filename firststack laststack inetcdf datadir outdir [digits]
!
! Optional argument digits can be used to round all floats to n significant
! digits in order to save disk space (used only for netcdf outputs).

! Example:
! combine_output7 salt.63 10 18 1 path/to/selfe/outputs/ some/path/
! combine_output7 salt.63 10 18 1 path/to/selfe/outputs/ some/path/ 4
!
! Inputs:
!        rank-specific binary files (from datadir/);
!        local_to_global_* (from datadir/);
!        additional inputs for non-standard outputs (hvel.67 etc) and binary format:
!             sidecenters.gr3, centers.gr3 (in datadir/../)
! Output: combined netcdf/binary file (e.g. *_salt.70 or *_salt.70.nc)
!
!  Compile on head nodes:
! ifort -O2 -o combine_output7 combine_output7.f90 selfe_geometry.f90 -assume byterecl -I/usr/local/include/ -L/usr/local/lib -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lm -lproj

!  Compile with gfortran:
! gfortran -O2 -g -o combine_output7 combine_output7.f90 selfe_geometry.f90 -I/usr/local/include/ -L/usr/local/lib -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lm -lproj

!  History: added netcdf4 with compression, UGRID CF naming, coordinate
!  transformation via libproj and command line arguments
!===============================================================================

module proj_bind
  !
  ! Module for interfacing between Proj-4 library libproj.a
  ! and Fortran. So far only pj_init_plus and pj_transform are supported.
  ! More routines can be added in a similar way.
  !
  use iso_c_binding
  implicit none
  private
  public :: pj_init_plus, pj_transform
  !
  interface
  function pj_init_plus(name) bind(c,name='pj_init_plus')
  use iso_c_binding
  implicit none
  type(c_ptr) :: pj_init_plus
  character(kind=c_char) :: name(*)
  end function pj_init_plus

  function pj_transform(src, dst, point_count, point_offset, &
  & x, y, z) bind(c,name='pj_transform')
  use iso_c_binding
  implicit none
  type(c_ptr), value :: src, dst
  integer(c_long), value :: point_count
  integer(c_int), value :: point_offset
  integer(c_int) :: pj_transform
  real(c_double) :: x, y, z
  end function pj_transform
  end interface
!
end module proj_bind

module convert_coords
  use iso_c_binding
  use proj_bind
  implicit none
  type(c_ptr) :: pj_ll, pj_wo, pj_spcs
  real(8), parameter :: RAD_TO_DEG = 180.0/3.14159265359
  real(8), parameter :: DEG_TO_RAD = 1.0/RAD_TO_DEG

  contains
    subroutine init_coord_sys()
      pj_spcs = pj_init_plus('+init=nad27:3601'//char(0))
      pj_ll = pj_init_plus('+proj=latlong +datum=WGS84'//char(0))
      pj_wo = pj_init_plus('+proj=latlong +nadgrids=WO'//char(0))
    end subroutine init_coord_sys
    subroutine convert_to_ll( x, y, z )
      implicit none
      real, intent(inout) :: x, y, z
      real(C_DOUBLE) xx,yy,zz
      integer(C_INT) res
      xx = x
      yy = y
      zz = z
      res = pj_transform(pj_spcs,pj_wo, 1_C_LONG, 0_C_INT, xx, yy, zz)
!       if ( res == 0 ) then
!         write(*,*) 'success',res,xx,yy
!       else
      if ( res /= 0 ) then
        write(*,*) 'failure',res,xx,yy
        res = pj_transform(pj_spcs,pj_ll, 1_C_LONG, 0_C_INT, xx, yy, zz)
      endif
      x = xx*RAD_TO_DEG
      y = yy*RAD_TO_DEG
      z = zz
    end subroutine convert_to_ll
    subroutine convert_to_spcs( x, y, z )
      implicit none
      real, intent(inout) :: x, y, z
      real(C_DOUBLE) xx,yy,zz
      integer(C_INT) res
      xx = x*DEG_TO_RAD
      yy = y*DEG_TO_RAD
      zz = z
      res = pj_transform(pj_wo,pj_spcs, 1_C_LONG, 0_C_INT, xx, yy, zz)
      if ( res == 0 ) then
        write(*,*) 'success',res,xx,yy
      else
        write(*,*) 'failure',res,xx,yy
        res = pj_transform(pj_ll,pj_spcs, 1_C_LONG, 0_C_INT, xx, yy, zz)
      endif
      x = xx
      y = yy
      z = zz
    end subroutine convert_to_spcs

end module convert_coords

integer function CK( iret )
  use netcdf
  integer,intent(in) :: iret
  if(iret.ne.NF90_NOERR) then
      print*, nf90_strerror(iret); stop
  endif
  CK=iret
end function CK

real function round( f, n )
! rounds f to n significant digits
  real,intent(in) :: f
  integer,intent(in) :: n
  real :: offset,precis,expo,bits,scal
  if (f == -9999. .or. f==0.0) then
    round = f
  else
    offset = 0.0
    if (abs(f) > 1e-16) then
      offset=int(floor(log10(abs(f))))
    endif
    precis = 10.**( offset-n )
    expo = log10(precis)
    if (expo < 0) then
      expo = int(floor(expo))
    else
      expo = int(ceiling(expo))
    endif
    bits = ceiling( log10( 10**(-expo) )/log10(2.0) )
    scal = 2.**bits
    round = nint(f*scal)/scal
  endif
end function round

program combine
!-------------------------------------------------------------------------------
!  use typeSizes
  use netcdf
  use convert_coords
  implicit real(4)(a-h,o-z),integer(i-n)
  include 'netcdf.inc'
  interface
    integer function CK( iret )
      integer,intent(in) :: iret
    end function CK
    real function round( f, n )
      real,intent(in) :: f
      integer,intent(in) :: n
    end function round
  end interface

  parameter(nbyte=4)
  character*30 file63
  character*12 it_char
  character*48 start_time,version,variable_nm,variable_dim
  character*48 data_format
  character*48 tbuf, c_time
  character*256 strtmp
  character*1 ctmp
  character*2 month, day
  character*4 year
  character(512) :: fgb,fgb2,fdb  ! Processor specific global output file name
  integer :: lfgb,lfdb       ! Length of processor specific global output file name
  character(len= 4) :: a_4
  allocatable ne(:),np(:),ns(:),ihot_len(:)
  allocatable ztot(:),sigma(:),cs(:),outb(:,:,:),eta2(:),outeb(:,:,:),outsb(:,:,:)
  allocatable i34(:),nm(:,:),nm2(:,:),js(:,:),xctr(:),yctr(:),dpe(:)
  allocatable x(:),y(:),dp(:),kbp00(:),iplg(:,:),ielg(:,:),kbp01(:,:)
  allocatable ylat(:),xlon(:),ycjlat(:),xcjlon(:),yctrlat(:),xctrlon(:)
  allocatable islg(:,:),kbs(:),kbe(:),xcj(:),ycj(:),dps(:),ic3(:,:),is(:,:),isidenode(:,:)
  allocatable nm_e(:,:),nm_s(:,:),eta2s(:),eta2e(:)
  ! command line args
  character*256   arg
  character*512  PATH
  character*512  OUTPATH
  integer first_stack, last_stack, nstacks
  character*4 data_loc ! node, side, cntr
  integer ihalf_lev ! 0 or 1
  logical useChunk
  
  !netcdf variables
  character*512 fname
  integer :: time_dims(1),ele_dims(2),x_dims(1),sigma_dims(1),var2d_dims(2),var3d_dims(3), &
            &data_start_2d(2),data_start_3d(3),data_count_2d(2),data_count_3d(3),z_dims(1), empty(0)
!-------------------------------------------------------------------------------
! Aquire user inputs
!-------------------------------------------------------------------------------
  call init_coord_sys()

  nargs = iargc()
  if ( nargs < 6 ) then
    write(*,*) 'Usage: '
    call getarg(0, arg)
    write(*,*) trim(arg)//' filename first_stack last_stack inetcdf datadir outputdir [ndigits]'
    stop
  endif
  call getarg(1, file63)
  call getarg(2, arg)
  read(arg,*) ibgn
  call getarg(3, arg)
  read(arg,*) iend
  call getarg(4, arg)
  read(arg,*) inc
  call getarg(5, PATH)
  call getarg(6, OUTPATH)
  if ( nargs == 7 ) then
    call getarg(7, arg)
    read(arg,*) ndigits
  else
    ndigits = -99
  endif

  write(*,*) 'Parsed arguments:'
  write(*,*) 'File:        ',file63
  write(*,*) 'Path:        ',trim(PATH)
  write(*,*) 'Output Dir:  ', trim(OUTPATH)
  write(*,*) 'First stack: ', ibgn
  write(*,*) 'Last stack:  ', iend
  write(*,*) 'NetCDF:      ', inc
  if (ndigits > -99) then
    write(*,*) 'Rounding decimals: ', ndigits
  endif

!   open(10,file='combine_output.in',status='old')
!   read(10,'(a30)') file63 !e.g. 'hvel.64'
!   read(10,*) ibgn,iend
!   read(10,*) inc !inc=1: netcdf option
!   close(10)

! Read local_to_global_0000 for global info
  open(10,file=trim(PATH)//'/local_to_global_0000',status='old')
  read(10,*)ne_global,np_global,nvrt,nproc,ntracers
  close(10)

  allocate(x(np_global),y(np_global),dp(np_global),kbp00(np_global),kbe(ne_global), &
           np(0:nproc-1),ns(0:nproc-1),ne(0:nproc-1),nm(ne_global,3),nm2(ne_global,3),eta2(np_global), &
           ztot(nvrt),sigma(nvrt),cs(nvrt),outb(np_global,nvrt,2),ihot_len(0:nproc-1), &
           outeb(ne_global,nvrt,2),dpe(ne_global),xctr(ne_global),yctr(ne_global), &
           eta2e(ne_global),ylat(np_global),xlon(np_global),yctrlat(ne_global),xctrlon(ne_global),stat=istat)
  if(istat/=0) stop 'Allocation error: x,y'

! Initialize outb for ivalid pts (below bottom etc)
  outb=-9999.
  outeb=-9999.

  ! compression parameters
  isuf = 1 ! SHUFFLE If non-zero, turn on the shuffle filter.
  icompr = 1 ! DEFLATE If non-zero, turn on the deflate filter.
  icompr_lev = 4 ! DEFLATE_LEVEL Must be between 0 and 9.
  ! chunking parameters (these have huge impact on I/O performance)
  nTimeChnk = 1   ! 1
  nNodeChnk = 512 ! 512 !large useful for write and slabs, bad for stations
  nVertChnk = 8  ! nvrt !large useful for stations, bad for slabs
  nCache = nVertChnk*nNodeChnk*nVertChnk*16
  
  useChunk = np_global > 10*nNodeChnk .and. nvrt > 2*nVertChnk

!-------------------------------------------------------------------------------
! Read rank-specific local_to_global*
!-------------------------------------------------------------------------------

  ! Compute ivs and itype; i23d_out for vis
  file63=adjustl(file63)
  lfile63=len_trim(file63)
  if(file63((lfile63-1):lfile63).eq.'61'.or.file63((lfile63-1):lfile63).eq.'63') then
    ivs=1
  else if(file63((lfile63-1):lfile63).eq.'62'.or.file63((lfile63-1):lfile63).eq.'64') then
    ivs=2
  else 
    write(it_char,'(i12)')ibgn
    it_char=adjustl(it_char)  !place blanks at end
    it_len=len_trim(it_char)  !length without trailing blanks
    fgb=trim(PATH)//'/'//it_char(1:it_len)//'_0000'; lfgb=len_trim(fgb)
    open(63,file=fgb(1:lfgb)//'_'//file63,access='direct',recl=nbyte,status='old')
    read(63,rec=3)ivs
    close(63)
    print*, 'ivs=',ivs
  endif
  if(file63((lfile63-1):lfile63).eq.'61'.or.file63((lfile63-1):lfile63).eq.'62') then
    itype=2; i23d_out=2
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'63'.or.file63((lfile63-1):lfile63).eq.'64') then
    itype=3; i23d_out=3
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'65') then
    itype=4; i23d_out=2 !2D side
    data_loc='side'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'66') then
    itype=5; i23d_out=2 !2D element
    data_loc='cntr'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'67') then
    itype=6; i23d_out=3 !3D side and whole level
    data_loc='side'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'68') then
    itype=7; i23d_out=3 !3D side and half level
    data_loc='side'; ihalf_lev=1
  else if(file63((lfile63-1):lfile63).eq.'69') then
    itype=8; i23d_out=3 !3D elem. and whole level
    data_loc='cntr'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'70') then
    itype=9; i23d_out=3 !3D elem. and half level
    data_loc='cntr'; ihalf_lev=1
  else if(file63((lfile63-1):lfile63).eq.'71') then
    itype=10; i23d_out=2 !2D node
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'72') then
    itype=11; i23d_out=3 !3D node and whole level
    data_loc='node'; ihalf_lev=0
  else if(file63((lfile63-1):lfile63).eq.'73') then
    itype=12; i23d_out=3 !3D node and half level
    data_loc='node'; ihalf_lev=1
  else 
    stop 'Unknown suffix'
  endif
   
  !Read sidecenters.gr3 or centers.gr3 for non-standard outputs to get conectivity tables
  if (inc == 0) then ! only for binary output
    if(itype==4.or.itype==6.or.itype==7) then
      open(14,file=trim(PATH)//'/../sidecenters.gr3',status='old')
      read(14,*); read(14,*)ns_e,ns1
      allocate(nm_s(ns_e,3),stat=istat)
      if(istat/=0) stop 'Allocation error: side grid'
      do i=1,ns1; read(14,*); enddo
      do i=1,ns_e
        read(14,*)j,k,nm_s(i,1:3)
      enddo !i
      close(14)
    endif !itype

    if(itype==5.or.itype==8.or.itype==9) then
      open(14,file=trim(PATH)//'/../centers.gr3',status='old')
      read(14,*); read(14,*)ne_e,ne1
      if(ne1/=ne_global) then
        print*, 'centers.gr3 not consistent with grid:',ne1,ne_global
        stop
      endif
      allocate(nm_e(ne_e,3),stat=istat)
      if(istat/=0) stop 'Allocation error: center grid'
      do i=1,ne1; read(14,*); enddo
      do i=1,ne_e
        read(14,*)j,k,nm_e(i,1:3)
      enddo !i
      close(14)
    endif !itype
  endif

  ! Read in local-global mappings from all ranks
  fdb=trim(PATH)//'/local_to_global_0000'
  lfdb=len_trim(fdb)

  !Find max. for dimensioning
  do irank=0,nproc-1
    write(fdb(lfdb-3:lfdb),'(i4.4)') irank
    open(10,file=fdb,status='old')
    read(10,*) !global info
    read(10,*) !info
    read(10,*)ne(irank)
    do i=1,ne(irank)
      read(10,*)!j,ielg(irank,i)
    enddo !i
    read(10,*)np(irank)
    do i=1,np(irank)
      read(10,*)
    enddo !i
    read(10,*)ns(irank)
    close(10)
  enddo !irank
  np_max=maxval(np(:))
  ns_max=maxval(ns(:))
  ne_max=maxval(ne(:))

  allocate(iplg(0:nproc-1,np_max),kbp01(0:nproc-1,np_max), &
     &ielg(0:nproc-1,ne_max),islg(0:nproc-1,ns_max),stat=istat)
  if(istat/=0) stop 'Allocation error (2)'

  !Re-read
  ns_global=0
  do irank=0,nproc-1
    write(fdb(lfdb-3:lfdb),'(i4.4)') irank
    open(10,file=fdb,status='old')

    read(10,*) !global info

    read(10,*) !info
    read(10,*)ne(irank)
    do i=1,ne(irank)
      read(10,*)j,ielg(irank,i)
    enddo !i
    read(10,*)np(irank)
    do i=1,np(irank)
      read(10,*)j,iplg(irank,i)
    enddo
    read(10,*)ns(irank) !sides
    do i=1,ns(irank)
      read(10,*)j,islg(irank,i)
      if(ns_global<islg(irank,i)) ns_global=islg(irank,i)
    enddo

    read(10,*) !'Header:'
    read(10,'(a)')data_format,version,start_time
    read(10,*)nrec,dtout,nspool,nvrt,kz,h0,h_s,h_c,theta_b,theta_f
    read(10,*)(ztot(k),k=1,kz-1),(sigma(k),k=1,nvrt-kz+1)
    read(10,*)np(irank),ne(irank),(x(iplg(irank,m)),y(iplg(irank,m)),dp(iplg(irank,m)),kbp01(irank,m),m=1,np(irank)), &
    &         (ntmp,(nm2(m,mm),mm=1,3),m=1,ne(irank))

    close(10)

!   Compute C(s) for output
    do klev=kz,nvrt
      k=klev-kz+1
      cs(k)=(1-theta_b)*sinh(theta_f*sigma(k))/sinh(theta_f)+ &
     &theta_b*(tanh(theta_f*(sigma(k)+0.5))-tanh(theta_f*0.5))/2/tanh(theta_f*0.5)
    enddo !klev

!   Compute kbp00 (to avoid mismatch of indices) - larger rank prevails
    do m=1,np(irank)
      ipgb=iplg(irank,m)
      kbp00(ipgb)=kbp01(irank,m)
    enddo !m
 
!   Reconstruct connectivity table
    do m=1,ne(irank)
      iegb=ielg(irank,m)
      if(iegb>ne_global) stop 'Overflow!'
      do mm=1,3
        itmp=nm2(m,mm)
        if(itmp>np(irank).or.itmp<=0) then
          write(*,*)'Overflow:',m,mm,itmp
          stop
        endif
        nm(iegb,mm)=iplg(irank,itmp)
      enddo !mm
    enddo !m
  enddo !irank=0,nproc-1

! Compute geometry
  call compute_nside(np_global,ne_global,nm,ns2)
  allocate(ic3(ne_global,3),js(ne_global,3),is(ns2,2),isidenode(ns2,2),xcj(ns2),ycj(ns2),ycjlat(ns2),xcjlon(ns2),stat=istat)
  if(istat/=0) stop 'Allocation error: side(0)'
  call selfe_geometry(np_global,ne_global,ns2,x,y,nm,ic3,js,is,isidenode,xcj,ycj)

  if(ns2/=ns_global) then
    write(*,*)'Mismatch in side:',ns2,ns_global
    stop
  endif

  if (inc == 0) then
    if(itype==4.or.itype==6.or.itype==7) then
      if(ns1/=ns2) then
        write(*,*)'Mismatch in side:',ns1,ns2
        stop
      endif
    endif
  endif

! Allocate side arrays
  allocate(dps(ns_global),kbs(ns_global),outsb(ns_global,nvrt,2),eta2s(ns_global),stat=istat)
  if(istat/=0) stop 'Allocation error: side'
  outsb=-9999.

! Compute side/element bottom index
  do i=1,ne_global
    kbe(i)=maxval(kbp00(nm(i,1:3)))
    dpe(i)=sum(dp(nm(i,1:3)))/3
    xctr(i)=sum(x(nm(i,1:3)))/3
    yctr(i)=sum(y(nm(i,1:3)))/3
  enddo !i
  do i=1,ns_global
    kbs(i)=maxval(kbp00(isidenode(i,1:2)))
    dps(i)=sum(dp(isidenode(i,1:2)))/2
  enddo !i

!     Compute node coordinates in lat/lon
  do i=1,np_global
    xtmp = x(i)
    ytmp = y(i)
    ztmp = 0.0
    call convert_to_ll( xtmp,ytmp,ztmp )
    xlon(i) = xtmp
    ylat(i) = ytmp
  enddo
  if ( data_loc == 'side' ) then
    do i=1,ns_global
      xtmp = xcj(i)
      ytmp = ycj(i)
      ztmp = 0.0
      call convert_to_ll( xtmp,ytmp,ztmp )
      xcjlon(i) = xtmp
      ycjlat(i) = ytmp
    enddo
  endif
  if ( data_loc == 'cntr' ) then
    do i=1,ne_global
      xtmp = xctr(i)
      ytmp = yctr(i)
      ztmp = 0.0
      call convert_to_ll( xtmp,ytmp,ztmp )
      xctrlon(i) = xtmp
      yctrlat(i) = ytmp
    enddo
  endif

! Compute record length to read from each rank-specific binary output per time step
  do irank=0,nproc-1
    ihot_len(irank)=nbyte*(2+np(irank)) !time,it,eta
    if(itype>3) ihot_len(irank)=ihot_len(irank)+nbyte !ivs

    if(itype==2.or.itype==10) then
      ihot_len(irank)=ihot_len(irank)+nbyte*ivs*np(irank)
    else if(itype==3) then
      do i=1,np(irank)
        do k=max0(1,kbp01(irank,i)),nvrt
          do m=1,ivs
            ihot_len(irank)=ihot_len(irank)+nbyte
          enddo !m
        enddo !k
      enddo !i
    else if(itype==4) then !2D side
      ihot_len(irank)=ihot_len(irank)+nbyte*ns(irank)*ivs
    else if(itype==5) then !2D elem
      ihot_len(irank)=ihot_len(irank)+nbyte*ne(irank)*ivs
    else if(itype==6.or.itype==7) then !3D side and whole/half level
      ihot_len(irank)=ihot_len(irank)+nbyte*ns(irank)*nvrt*ivs
    else if(itype==8.or.itype==9) then !3D element and whole/half level
      ihot_len(irank)=ihot_len(irank)+nbyte*ne(irank)*nvrt*ivs
    else if(itype==11.or.itype==12) then !3D node and whole/half level
      ihot_len(irank)=ihot_len(irank)+nbyte*np(irank)*nvrt*ivs
    endif
  enddo !irank

!-------------------------------------------------------------------------------
! Time iteration -- select "node" data
!-------------------------------------------------------------------------------

! Loop over input files
  do iinput=ibgn,iend
    write(it_char,'(i12)')iinput
    it_char=adjustl(it_char)  !place blanks at end
    it_len=len_trim(it_char)  !length without trailing blanks
    fgb=trim(PATH)//'/'//it_char(1:it_len)//'_0000'; lfgb=len_trim(fgb);

    !Write header to output files 
    if(inc==0) then
      open(65,file=trim(OUTPATH)//'/'//it_char(1:it_len)//'_'//file63,status='replace')
      data_format='DataFormat v5.0'
      variable_nm=file63 !not important
      variable_dim=file63

      write(65,'(a48)',advance="no") data_format
      write(65,'(a48)',advance="no") version
      write(65,'(a48)',advance="no") start_time
      write(65,'(a48)',advance="no") variable_nm
      write(65,'(a48)',advance="no") variable_dim

      a_4 = transfer(source=nrec,mold=a_4)
      write(65,"(a4)",advance="no") a_4
      a_4 = transfer(source=dtout,mold=a_4)
      write(65,"(a4)",advance="no") a_4
      a_4 = transfer(source=nspool,mold=a_4)
      write(65,"(a4)",advance="no") a_4
      a_4 = transfer(source=ivs,mold=a_4)
      write(65,"(a4)",advance="no") a_4
      a_4 = transfer(source=i23d_out,mold=a_4)
      write(65,"(a4)",advance="no") a_4

      !Vertical grid
      a_4 = transfer(source=nvrt,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=kz,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=h0,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=h_s,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=h_c,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=theta_b,mold=a_4)
      write(65,'(a4)',advance="no") a_4
      a_4 = transfer(source=theta_f,mold=a_4)
      write(65,'(a4)',advance="no") a_4

      do k=1,kz-1
        a_4 = transfer(source=ztot(k),mold=a_4)
        write(65,'(a4)',advance="no") a_4
      enddo
      do k=kz,nvrt
        kin=k-kz+1
        a_4 = transfer(source=sigma(kin),mold=a_4)
        write(65,'(a4)',advance="no") a_4
      enddo !k

      !Horizontal grid
      if(itype<=3.or.itype>=10) then !standard
        a_4 = transfer(source=np_global,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        a_4 = transfer(source=ne_global,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        do m=1,np_global
          a_4 = transfer(source=x(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=y(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=dp(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=kbp00(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
        enddo !m=1,np
        do m=1,ne_global
          a_4 = transfer(source=3,mold=a_4)
          write(65,'(a4)',advance="no") a_4
          do mm=1,3
            a_4 = transfer(source=nm(m,mm),mold=a_4)
            write(65,'(a4)',advance="no") a_4
          enddo !mm
        enddo !m
      else if(itype==4.or.itype==6.or.itype==7) then !side based
        a_4 = transfer(source=ns_global,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        a_4 = transfer(source=ns_e,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        do m=1,ns_global
          a_4 = transfer(source=xcj(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=ycj(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=dps(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=kbs(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
        enddo !m
        do m=1,ns_e
          a_4 = transfer(source=3,mold=a_4)
          write(65,'(a4)',advance="no") a_4
          do mm=1,3
            a_4 = transfer(source=nm_s(m,mm),mold=a_4)
            write(65,'(a4)',advance="no") a_4
          enddo !mm
        enddo !m
      else !elem. based
        a_4 = transfer(source=ne_global,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        a_4 = transfer(source=ne_e,mold=a_4)
        write(65,'(a4)',advance="no") a_4
        do m=1,ne_global
          a_4 = transfer(source=xctr(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=yctr(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=dpe(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
          a_4 = transfer(source=kbe(m),mold=a_4)
          write(65,'(a4)',advance="no") a_4
        enddo !m
        do m=1,ne_e
          a_4 = transfer(source=3,mold=a_4)
          write(65,'(a4)',advance="no") a_4
          do mm=1,3
            a_4 = transfer(source=nm_e(m,mm),mold=a_4)
            write(65,'(a4)',advance="no") a_4
          enddo !mm
        enddo !m
      endif !itype

    else !netcdf
!     enter define mode
      fname=trim(OUTPATH)//'/'//it_char(1:it_len)//'_'//file63(1:lfile63)//'.nc'
      write(*,*) 'generating file ',trim(fname)
      ir=CK(nf90_create(trim(fname), NF90_NETCDF4, ncid))
!     define dimensions
      ir=CK(nf90_def_dim(ncid, 'node',np_global, node_dim))
      ir=CK(nf90_def_dim(ncid, 'face',ne_global, nface_dim))
      if ( data_loc=='side' ) then
        ir=CK(nf90_def_dim(ncid, 'edge',ns_global, nedge_dim))
      endif
      ir=CK(nf90_def_dim(ncid, 'max_face_nodes',3, nfacenode_dim))
      ir=CK(nf90_def_dim(ncid, 'layers',nvrt, layers_dim))
      ir=CK(nf90_def_dim(ncid, 'sigma',nvrt-kz+1, nsigma_dim))
      if(kz/=1) ir=CK(nf90_def_dim(ncid, 'nz',kz, nz_dim))
      ir=CK(nf90_def_dim(ncid, 'time', NF90_UNLIMITED, ntime_dim))
      ir=CK(nf90_def_dim(ncid, 'One', 1, n_one_dim))
      ir=CK(nf90_def_dim(ncid, 'Two', 2, n_two_dim))
      if ( data_loc=='node' ) then
        ndata_dim = node_dim
      else if ( data_loc=='side' ) then
        ndata_dim = nedge_dim
      else if ( data_loc=='cntr' ) then
        ndata_dim = nface_dim
      endif

!     define variables
      ir=CK(nf90_def_var(ncid,'Mesh',NF90_INT,varid=itopo_id))
      ir=CK(nf90_put_att(ncid,itopo_id,'cf_role','mesh_topology'))
      ir=CK(nf90_put_att(ncid,itopo_id,'long_name','Topology data of 2D unstructured mesh'))
      ir=CK(nf90_put_att(ncid,itopo_id,'topology_dimension',2))
      ir=CK(nf90_put_att(ncid,itopo_id,'node_coordinates','node_lon node_lat'))
      ir=CK(nf90_put_att(ncid,itopo_id,'face_node_connectivity','face_nodes'))
      ! attribute required if variables will be defined on edges
      if ( data_loc=='side' ) then
        ir=CK(nf90_put_att(ncid,itopo_id,'edge_node_connectivity','edge_nodes'))
        ! optional attribute (requires edge_node_connectivity)
!         ir=CK(nf90_put_att(ncid,itopo_id,'edge_coordinates','edge_x edge_y')
        ir=CK(nf90_put_att(ncid,itopo_id,'edge_coordinates','edge_lon edge_lat'))
      endif
      if ( data_loc == 'cntr' ) then
      ! optional attribute
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_coordinates','face_x face_y'))
        ir=CK(nf90_put_att(ncid,itopo_id,'face_coordinates','face_lon face_lat'))
      ! optional attribute (requires edge_node_connectivity)
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_edge_connectivity','Mesh_face_edges'))
      ! optional attribute
!         ir=CK(nf90_put_att(ncid,itopo_id,'face_face_connectivity','Mesh_face_links'))
      endif

      ir=CK(nf90_def_var(ncid,'Lambert_Conformal',NF90_INT,varid=iproj_id))
      ir=CK(nf90_put_att(ncid,iproj_id,'grid_mapping_name','lambert_conformal_conic'))
      ir=CK(nf90_put_att(ncid,iproj_id,'standard_parallel',(/44.333333,46.000000/)))
      ir=CK(nf90_put_att(ncid,iproj_id,'longitude_of_central_meridian',-120.500000))
      ir=CK(nf90_put_att(ncid,iproj_id,'latitude_of_projection_origin',43.666667))
      ir=CK(nf90_put_att(ncid,iproj_id,'false_easting',609601.219202))
      ir=CK(nf90_put_att(ncid,iproj_id,'false_northing',0))
      ir=CK(nf90_put_att(ncid,iproj_id,'comment','ORSPCS-N NAD27'))

      ir=CK(nf90_def_var(ncid,'vert_coords',NF90_INT,varid=ivert_id))
      ir=CK(nf90_put_att(ncid,ivert_id,'standard_name','ocean_s_z_coordinate'))
      ir=CK(nf90_put_att(ncid,ivert_id,'long_name','S-Z Levels'))
      ir=CK(nf90_put_att(ncid,ivert_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ivert_id,'positive','up'))
      strtmp = 'eta:elev depth:depth zlev:z s:sigma C:Cs h_c:h_c'
      ir=CK(nf90_put_att(ncid,ivert_id,'formula_terms',strtmp))

      ir=CK(nf90_def_var(ncid,'time',NF90_FLOAT,ntime_dim,itime_id))
      ! time:units="seconds since 2005-05-21 00:00:00 -8:00";
      c_time = start_time
      READ (c_time, "(a2,a1,a2,a1,a4)") month, ctmp, day, ctmp, year
!       write(*,*)year,'-',month,'-',day,' 00:00:00 -8:00'
      tbuf = 'seconds since ' // year // '-' // month // '-' // day // ' 00:00:00 -8:00'
      ir=CK(nf90_put_att(ncid,itime_id,'standard_name','time'))
      ir=CK(nf90_put_att(ncid,itime_id,'long_name','Time'))
      ir=CK(nf90_put_att(ncid,itime_id,'units','seconds'))
      ir=CK(nf90_put_att(ncid,itime_id,'base_date',trim(tbuf)))

      ir=CK(nf90_def_var(ncid,'face_nodes',NF90_INT,(/ nfacenode_dim, nface_dim /),iconn_id))
!       ir=CK(nf90_def_var_chunking(ncid,iconn_id, NF90_CHUNKED, (/3,nNodeChnk/))
      ir=CK(nf90_def_var_deflate(ncid,iconn_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iconn_id,'cf_role','face_node_connectivity'))
      ir=CK(nf90_put_att(ncid,iconn_id,'long_name','Maps every face to its corner nodes.'))
      ir=CK(nf90_put_att(ncid,iconn_id,'start_index',1))
      ir=CK(nf90_def_var_fill(ncid, iconn_id, 0, 9999999))

      if ( data_loc=='side' ) then
        ir=CK(nf90_def_var(ncid,'edge_nodes',NF90_INT,(/ n_two_dim, nedge_dim /),ieconn_id))
!         ir=CK(nf90_def_var_chunking(ncid,ieconn_id, NF90_CHUNKED, (/2,nNodeChnk/))
        ir=CK(nf90_def_var_deflate(ncid,ieconn_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ieconn_id,'cf_role','edge_node_connectivity'))
        ir=CK(nf90_put_att(ncid,ieconn_id,'long_name','Maps every edge to the two end nodes'))
        ir=CK(nf90_put_att(ncid,ieconn_id,'start_index',1))
        ir=CK(nf90_def_var_fill(ncid, ieconn_id, 0, 9999999))

        ir=CK(nf90_def_var(ncid,'edge_x',NF90_DOUBLE,nedge_dim,iex_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,iex_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,iex_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,iex_id,'standard_name','projection_x_coordinate'))
        ir=CK(nf90_put_att(ncid,iex_id,'long_name','x-coordinates of edge midpoints'))
        ir=CK(nf90_put_att(ncid,iex_id,'units','m'))
        ir=CK(nf90_put_att(ncid,iex_id,'axis','X'))

        ir=CK(nf90_def_var(ncid,'edge_y',NF90_DOUBLE,nedge_dim,iey_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,iey_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,iey_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,iey_id,'standard_name','projection_y_coordinate'))
        ir=CK(nf90_put_att(ncid,iey_id,'long_name','y-coordinates of edge midpoints'))
        ir=CK(nf90_put_att(ncid,iey_id,'units','m'))
        ir=CK(nf90_put_att(ncid,iey_id,'axis','Y'))

        ir=CK(nf90_def_var(ncid,'edge_lon',NF90_DOUBLE,nedge_dim,ielon_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,ielon_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,ielon_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ielon_id,'standard_name','longitude'))
        ir=CK(nf90_put_att(ncid,ielon_id,'long_name','Longitude of edge midpoints'))
        ir=CK(nf90_put_att(ncid,ielon_id,'units','degrees_east'))

        ir=CK(nf90_def_var(ncid,'edge_lat',NF90_DOUBLE,nedge_dim,ielat_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,ielat_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,ielat_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ielat_id,'standard_name','latitude'))
        ir=CK(nf90_put_att(ncid,ielat_id,'long_name','Latitude of edge midpoints'))
        ir=CK(nf90_put_att(ncid,ielat_id,'units','degrees_north'))

      endif

      if ( data_loc=='cntr' ) then
        ir=CK(nf90_def_var(ncid,'face_x',NF90_DOUBLE,nface_dim,ifx_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,ifx_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,ifx_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ifx_id,'standard_name','projection_x_coordinate'))
        ir=CK(nf90_put_att(ncid,ifx_id,'long_name','x-coordinates of face centers'))
        ir=CK(nf90_put_att(ncid,ifx_id,'units','m'))
        ir=CK(nf90_put_att(ncid,ifx_id,'axis','X'))

        ir=CK(nf90_def_var(ncid,'face_y',NF90_DOUBLE,nface_dim,ify_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,ify_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,ify_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ify_id,'standard_name','projection_y_coordinate'))
        ir=CK(nf90_put_att(ncid,ify_id,'long_name','y-coordinates of face centers'))
        ir=CK(nf90_put_att(ncid,ify_id,'units','m'))
        ir=CK(nf90_put_att(ncid,ify_id,'axis','Y'))

        ir=CK(nf90_def_var(ncid,'face_lon',NF90_DOUBLE,nface_dim,iflon_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,iflon_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,iflon_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,iflon_id,'standard_name','longitude'))
        ir=CK(nf90_put_att(ncid,iflon_id,'long_name','Longitude of face centers'))
        ir=CK(nf90_put_att(ncid,iflon_id,'units','degrees_east'))

        ir=CK(nf90_def_var(ncid,'face_lat',NF90_DOUBLE,nface_dim,iflat_id))
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,iflat_id, NF90_CHUNKED, (/nNodeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,iflat_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,iflat_id,'standard_name','latitude'))
        ir=CK(nf90_put_att(ncid,iflat_id,'long_name','Latitude of face centers'))
        ir=CK(nf90_put_att(ncid,iflat_id,'units','degrees_north'))
      endif

      ir=CK(nf90_def_var(ncid,'node_x',NF90_DOUBLE,node_dim,ix_id))
      if (useChunk) then
        ir=CK(nf90_def_var_chunking(ncid,ix_id, NF90_CHUNKED, (/nNodeChnk/)))
      endif
      ir=CK(nf90_def_var_deflate(ncid,ix_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ix_id,'standard_name','projection_x_coordinate'))
      ir=CK(nf90_put_att(ncid,ix_id,'long_name','x-coordinates of 2D mesh nodes'))
      ir=CK(nf90_put_att(ncid,ix_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ix_id,'axis','X'))

      ir=CK(nf90_def_var(ncid,'node_y',NF90_DOUBLE,node_dim,iy_id))
      if (useChunk) then
        ir=CK(nf90_def_var_chunking(ncid,iy_id, NF90_CHUNKED, (/nNodeChnk/)))
      endif
      ir=CK(nf90_def_var_deflate(ncid,iy_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,iy_id,'standard_name','projection_y_coordinate'))
      ir=CK(nf90_put_att(ncid,iy_id,'long_name','y-coordinates of 2D mesh nodes'))
      ir=CK(nf90_put_att(ncid,iy_id,'units','m'))
      ir=CK(nf90_put_att(ncid,iy_id,'axis','Y'))

      ir=CK(nf90_def_var(ncid,'node_lon',NF90_DOUBLE,node_dim,ilon_id))
      if (useChunk) then
        ir=CK(nf90_def_var_chunking(ncid,ilon_id, NF90_CHUNKED, (/nNodeChnk/)))
      endif
      ir=CK(nf90_def_var_deflate(ncid,ilon_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ilon_id,'standard_name','longitude'))
      ir=CK(nf90_put_att(ncid,ilon_id,'long_name','Longitude of 2D mesh nodes'))
      ir=CK(nf90_put_att(ncid,ilon_id,'units','degrees_east'))

      ir=CK(nf90_def_var(ncid,'node_lat',NF90_DOUBLE,node_dim,ilat_id))
      if (useChunk) then
        ir=CK(nf90_def_var_chunking(ncid,ilat_id, NF90_CHUNKED, (/nNodeChnk/)))
      endif
      ir=CK(nf90_def_var_deflate(ncid,ilat_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ilat_id,'standard_name','latitude'))
      ir=CK(nf90_put_att(ncid,ilat_id,'long_name','Latitude of 2D mesh nodes'))
      ir=CK(nf90_put_att(ncid,ilat_id,'units','degrees_north'))

      ir=CK(nf90_def_var(ncid,'sigma',NF90_DOUBLE,nsigma_dim,isigma_id))
      ir=CK(nf90_put_att(ncid,isigma_id,'long_name','S coordinates at whole levels'))
      ir=CK(nf90_put_att(ncid,isigma_id,'units','non-dimensional'))
      ir=CK(nf90_put_att(ncid,isigma_id,'positive','up'))

      if(kz/=1) then
        ir=CK(nf90_def_var(ncid,'z',NF90_FLOAT,nz_dim,iz_id))
        ir=CK(nf90_put_att(ncid,iz_id,'long_name','Z coordinates at whole levels'))
        ir=CK(nf90_put_att(ncid,iz_id,'units','m'))
        ir=CK(nf90_put_att(ncid,iz_id,'positive','up'))
      endif

      ir=CK(nf90_def_var(ncid,'depth',NF90_FLOAT,ndata_dim,idepth_id))
      if (data_loc=='node') then
        ir=CK(nf90_put_att(ncid,idepth_id,'location','node'))
        ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','node_lon node_lat'))
      else if (data_loc=='side') then
        ir=CK(nf90_put_att(ncid,idepth_id,'location','edge'))
        ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','edge_lon edge_lat'))
      else if (data_loc=='cntr') then
        ir=CK(nf90_put_att(ncid,idepth_id,'location','face'))
        ir=CK(nf90_put_att(ncid,idepth_id,'coordinates','face_lon face_lat'))
      endif
      if (useChunk) then
        ir=CK(nf90_def_var_chunking(ncid,idepth_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
      endif
      ir=CK(nf90_def_var_deflate(ncid,idepth_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,idepth_id,'standard_name','sea_floor_depth_below_sea_level'))
      ir=CK(nf90_put_att(ncid,idepth_id,'long_name','Bathymetry'))
      ir=CK(nf90_put_att(ncid,idepth_id,'units','m'))
      ir=CK(nf90_put_att(ncid,idepth_id,'positive','down'))
      ir=CK(nf90_put_att(ncid,idepth_id,'missing_value',-9999.))
      ir=CK(nf90_put_att(ncid,idepth_id,'mesh','Mesh'))

      ir=CK(nf90_def_var(ncid,'elev',NF90_FLOAT,(/ ndata_dim, ntime_dim /),ielev_id))
      if (data_loc=='node') then
        ir=CK(nf90_put_att(ncid,ielev_id,'location','node'))
        ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','node_lon node_lat'))
      else if (data_loc=='side') then
        ir=CK(nf90_put_att(ncid,ielev_id,'location','edge'))
        ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','edge_lon edge_lat'))
      else if (data_loc=='cntr') then
        ir=CK(nf90_put_att(ncid,ielev_id,'location','face'))
        ir=CK(nf90_put_att(ncid,ielev_id,'coordinates','face_lon face_lat'))
      endif
        if (useChunk) then
          ir=CK(nf90_def_var_chunking(ncid,ielev_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
        endif
        ir=CK(nf90_def_var_deflate(ncid,ielev_id,isuf,icompr,icompr_lev))
        ir=CK(nf90_put_att(ncid,ielev_id,'standard_name','sea_surface_height_above_sea_level'))
        ir=CK(nf90_put_att(ncid,ielev_id,'long_name','Sea surface elevation'))
        ir=CK(nf90_put_att(ncid,ielev_id,'units','m'))
        ir=CK(nf90_put_att(ncid,ielev_id,'positive','up'))
        ir=CK(nf90_put_att(ncid,ielev_id,'missing_value',-9999.))
        ir=CK(nf90_put_att(ncid,ielev_id,'mesh','Mesh'))

      ir=CK(nf90_def_var(ncid,'Cs',NF90_FLOAT,nsigma_dim,ics_id))
      ir=CK(nf90_put_att(ncid,ics_id,'long_name','Function C(s) at whole levels'))
      ir=CK(nf90_put_att(ncid,ics_id,'units','non-dimensional'))
      ir=CK(nf90_put_att(ncid,ics_id,'positive','up'))

      ir=CK(nf90_def_var(ncid,'h_c',NF90_FLOAT,n_one_dim,ihc_id))
      ir=CK(nf90_put_att(ncid,ihc_id,'long_name','S-Z coordinate surface layer depth'))
      ir=CK(nf90_put_att(ncid,ihc_id,'units','m'))
      ir=CK(nf90_put_att(ncid,ihc_id,'positive','down'))

      ir=CK(nf90_def_var(ncid,'h0',NF90_FLOAT,n_one_dim,ih0_id))
      ir=CK(nf90_put_att(ncid,ih0_id,'long_name','Wet-dry threshold depth'))
      ir=CK(nf90_put_att(ncid,ih0_id,'units','m'))

      ir=CK(nf90_def_var(ncid,'k_bottom',NF90_INT,ndata_dim,ikb_id))
      ir=CK(nf90_def_var_deflate(ncid,ikb_id,isuf,icompr,icompr_lev))
      ir=CK(nf90_put_att(ncid,ikb_id,'standard_name','model_level_number_at_sea_floor'))
      ir=CK(nf90_put_att(ncid,ikb_id,'long_name','bottom level index'))
      ir=CK(nf90_put_att(ncid,ikb_id,'units','1'))
      ir=CK(nf90_put_att(ncid,ikb_id,'positive','down'))

      if(i23d_out==2 ) then
        if ( file63(1:lfile63-3) /= 'elev' ) then
          write(*,*) 'this is a 2D var'
          var2d_dims(1)=ndata_dim
          var2d_dims(2)=ntime_dim
          if ( ivs == 1 ) then
            ir=CK(nf90_def_var(ncid,file63(1:lfile63-3),NF90_FLOAT,var2d_dims,ivar_id))
            if (useChunk) then
              ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
            endif
            ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
          else
            ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_x',NF90_FLOAT,var2d_dims,ivar_id))
            if (useChunk) then
              ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
            endif
            ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
            ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_y',NF90_FLOAT,var2d_dims,ivar2_id))
            if (useChunk) then
              ir=CK(nf90_def_var_chunking(ncid,ivar2_id, NF90_CHUNKED, (/nNodeChnk,nTimeChnk/)))
            endif
            ir=CK(nf90_def_var_deflate(ncid,ivar2_id,isuf,icompr,icompr_lev))
          endif
          ! set standard_name and units
          if ( file63(1:lfile63-3) == 'pres' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','air_pressure_at_sea_level'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','Pa'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air pressure at sea surface'))
          else if ( file63(1:lfile63-3) == 'airt' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_temperature'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','degree_Celsius'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air temperature at sea surface'))
          else if ( file63(1:lfile63-3) == 'shum' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_specific_humidity'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Air humidity at sea surface'))
          else if ( file63(1:lfile63-3) == 'srad' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downwelling_shortwave_flux_in_air'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Solar radiation'))
          else if ( file63(1:lfile63-3) == 'flsu' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upward_sensible_heat_flux'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Sensible heat flux at sea surface'))
          else if ( file63(1:lfile63-3) == 'fllu' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upward_latent_heat_flux'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Latent heat flux at sea surface'))
          else if ( file63(1:lfile63-3) == 'radu' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_upwelling_longwave_flux_in_air'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Upwelling infrared (longwave) radiative flux at sea surface'))
          else if ( file63(1:lfile63-3) == 'radd' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downwelling_longwave_flux_in_air'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Downwelling infrared (longwave) radiative flux at sea surface'))
          else if ( file63(1:lfile63-3) == 'flux' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','downward_heat_flux_in_air'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','W m-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Net downward heat flux at sea surface (except for solar)'))
          else if ( file63(1:lfile63-3) == 'evap' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','water_evaporation_flux'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-2 s-1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Evaporation rate'))
          else if ( file63(1:lfile63-3) == 'prcp' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','precipitation_flux'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-2 s-1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Precipitation rate'))
          else if ( file63(1:lfile63-3) == 'dahv' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_depth_averaged_velocity')) ! No standard
            ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Depth averaged x velocty'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_depth_averaged_velocity')) ! No standard
            ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Depth averaged y velocty'))
          else if ( file63(1:lfile63-3) == 'dihv' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_depth_integrated_velocity')) ! No standard
            ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Depth integrated x velocty'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_depth_integtated_velocity')) ! No standard
            ir=CK(nf90_put_att(ncid,ivar2_id,'units','m2 s-1'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Depth integrated y velocty'))
          else if ( file63(1:lfile63-3) == 'wind' ) then
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','x_wind'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Wind x velocity'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','y_wind'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Wind y velocity'))
          else if ( file63(1:lfile63-3) == 'wist' ) then ! TODO check
            ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','surface_downward_x_stress'))
            ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-2'))
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Wind induced momentum x flux'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','surface_downward_y_stress'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'units','m2 s-2'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','Wind induced momentum y flux'))
          else
            write(*,*) 'Unknown 2D variable ', file63(1:lfile63-3)
            ir=CK(nf90_put_att(ncid,ivar_id,'long_name',file63(1:lfile63-3)))
          endif
          ir=CK(nf90_put_att(ncid,ivar_id,'missing_value',-9999.))
          ir=CK(nf90_put_att(ncid,ivar_id,'mesh','Mesh'))
          if (data_loc=='node') then
            ir=CK(nf90_put_att(ncid,ivar_id,'location','node'))
            ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','node_lon node_lat'))
          else if (data_loc=='side') then
            ir=CK(nf90_put_att(ncid,ivar_id,'location','edge'))
            ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','edge_lon edge_lat'))
          else if (data_loc=='cntr') then
            ir=CK(nf90_put_att(ncid,ivar_id,'location','face'))
            ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','face_lon face_lat'))
          endif
          if ( ivs == 2 ) then
            ir=CK(nf90_put_att(ncid,ivar2_id,'missing_value',-9999.))
            ir=CK(nf90_put_att(ncid,ivar2_id,'mesh','Mesh'))
            if (data_loc=='node') then
              ir=CK(nf90_put_att(ncid,ivar2_id,'location','node'))
              ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','node_lon node_lat'))
            else if (data_loc=='side') then
              ir=CK(nf90_put_att(ncid,ivar2_id,'location','edge'))
              ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','edge_lon edge_lat'))
            else if (data_loc=='cntr') then
              ir=CK(nf90_put_att(ncid,ivar2_id,'location','face'))
              ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','face_lon face_lat'))
            endif
          endif
        endif
      else !3D
        write(*,*) 'this is a 3D var'
        var3d_dims(1)=ndata_dim
        var3d_dims(2)=layers_dim; var3d_dims(3)=ntime_dim
        if ( ivs == 1 ) then
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3),NF90_FLOAT,var3d_dims,ivar_id))
          if (useChunk) then
            ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
          endif
          ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
        else
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_x',NF90_FLOAT,var3d_dims,ivar_id))
          if (useChunk) then
            ir=CK(nf90_def_var_chunking(ncid,ivar_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
          endif
          ir=CK(nf90_def_var_deflate(ncid,ivar_id,isuf,icompr,icompr_lev))
          ir=CK(nf90_def_var(ncid,file63(1:lfile63-3)//'_y',NF90_FLOAT,var3d_dims,ivar2_id))
          if (useChunk) then
            ir=CK(nf90_def_var_chunking(ncid,ivar2_id, NF90_CHUNKED, (/nNodeChnk,nVertChnk,nTimeChnk/)))
          endif
          ir=CK(nf90_def_var_deflate(ncid,ivar2_id,isuf,icompr,icompr_lev))
        endif
        if ( file63(1:lfile63-3) == 'salt' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_salinity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','1e-3'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Salinity'))
        else if ( file63(1:lfile63-3) == 'temp' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_temperature'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','degree_Celsius'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Temperature'))
        else if ( file63(1:lfile63-3) == 'vert' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','upward_sea_water_velocity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical velocity component'))
        else if ( file63(1:lfile63-3) == 'vdff' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_momentum_diffusivity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy viscosty'))
        else if ( file63(1:lfile63-3) == 'tdff' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_tracer_diffusivity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy diffusivity of tracers'))
        else if ( file63(1:lfile63-3) == 'kine' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_eddy_kinetic_energy')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m2 s-2'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical eddy kinetic energy'))
        else if ( file63(1:lfile63-3) == 'mixl' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','ocean_vertical_eddy_mixing_length')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical turbulent mixing lenght'))

        else if ( file63(1:lfile63-3) == 'conc' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_density'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','kg m-3'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Water density'))
        else if ( file63(1:lfile63-3) == 'zcor' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','model_z_coordinate')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','Vertical coordinates'))
        else if ( file63(1:lfile63-3) == 'qnon' ) then ! TODO check
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','normalized_non_hydrostatic_pressure')) ! No standard
          ir=CK(nf90_put_att(ncid,ivar_id,'units','Pa'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','normalized non-hydrostatic pressure'))

        else if ( file63(1:lfile63-3) == 'hvel' ) then
          ir=CK(nf90_put_att(ncid,ivar_id,'standard_name','sea_water_x_velocity'))
          ir=CK(nf90_put_att(ncid,ivar_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name','horizontal y velocity component'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'standard_name','sea_water_y_velocity'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'units','m s-1'))
          ir=CK(nf90_put_att(ncid,ivar2_id,'long_name','horizontal y velocity component'))
        else
          write(*,*) 'Unknown 3D variable ', file63(1:lfile63-3)
          ir=CK(nf90_put_att(ncid,ivar_id,'long_name',file63(1:lfile63-3)))
        endif
        ir=CK(nf90_put_att(ncid,ivar_id,'missing_value',-9999.))
        ir=CK(nf90_put_att(ncid,ivar_id,'mesh','Mesh'))
        if (data_loc=='node') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','node'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','node_lon node_lat'))
        else if (data_loc=='side') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','edge'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','edge_lon edge_lat'))
        else if (data_loc=='cntr') then
          ir=CK(nf90_put_att(ncid,ivar_id,'location','face'))
          ir=CK(nf90_put_att(ncid,ivar_id,'coordinates','face_lon face_lat'))
        endif
        if ( ivs == 2 ) then
          ir=CK(nf90_put_att(ncid,ivar2_id,'missing_value',-9999.))
          ir=CK(nf90_put_att(ncid,ivar2_id,'mesh','Mesh'))
          if (data_loc=='node') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','node'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','node_lon node_lat'))
          else if (data_loc=='side') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','edge'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','edge_lon edge_lat'))
          else if (data_loc=='cntr') then
            ir=CK(nf90_put_att(ncid,ivar2_id,'location','face'))
            ir=CK(nf90_put_att(ncid,ivar2_id,'coordinates','face_lon face_lat'))
          endif
        endif
      endif

      ir=CK(nf90_put_att(ncid, NF90_GLOBAL, 'Conventions', 'UGRID CF'))
!     leave define mode
      ir=CK(nf90_enddef(ncid))

!     Write mode (header part only)
      ir=CK(nf90_put_var(ncid,iconn_id,transpose(nm),(/1,1/),(/3,ne_global/)))
      if ( data_loc=='side' ) then
        ir=CK(nf90_put_var(ncid,ieconn_id,transpose(isidenode),(/1,1/),(/2,ns_global/)))
      endif
      ir=CK(nf90_put_var(ncid,ix_id,x,(/1/),(/np_global/)))
      ir=CK(nf90_put_var(ncid,iy_id,y,(/1/),(/np_global/)))
      ir=CK(nf90_put_var(ncid,ilon_id,xlon,(/1/),(/np_global/)))
      ir=CK(nf90_put_var(ncid,ilat_id,ylat,(/1/),(/np_global/)))
      if ( data_loc == 'side' ) then
        ir=CK(nf90_put_var(ncid,iex_id,xcj,(/1/),(/ns_global/)))
        ir=CK(nf90_put_var(ncid,iey_id,ycj,(/1/),(/ns_global/)))
        ir=CK(nf90_put_var(ncid,ielon_id,xcjlon,(/1/),(/ns_global/)))
        ir=CK(nf90_put_var(ncid,ielat_id,ycjlat,(/1/),(/ns_global/)))
      endif
      if ( data_loc == 'cntr' ) then
        ir=CK(nf90_put_var(ncid,ifx_id,xctr,(/1/),(/ne_global/)))
        ir=CK(nf90_put_var(ncid,ify_id,yctr,(/1/),(/ne_global/)))
        ir=CK(nf90_put_var(ncid,iflon_id,xctrlon,(/1/),(/ne_global/)))
        ir=CK(nf90_put_var(ncid,iflat_id,yctrlat,(/1/),(/ne_global/)))
      endif
      if (data_loc=='node') then
        ir=CK(nf90_put_var(ncid,idepth_id,dp,(/1/),(/np_global/)))
        ir=CK(nf90_put_var(ncid,ikb_id,kbp00,(/1/),(/np_global/)))
      else if (data_loc=='side') then ! TODO change dp
        ir=CK(nf90_put_var(ncid,idepth_id,dps,(/1/),(/ns_global/)))
        ir=CK(nf90_put_var(ncid,ikb_id,kbs,(/1/),(/ns_global/)))
      else if (data_loc=='cntr') then
        ir=CK(nf90_put_var(ncid,idepth_id,dpe,(/1/),(/ne_global/)))
        ir=CK(nf90_put_var(ncid,ikb_id,kbe,(/1/),(/ne_global/)))
      endif
      ir=CK(nf90_put_var(ncid,isigma_id,sigma,(/1/),(/nvrt-kz+1/)))
      ir=CK(nf90_put_var(ncid,ics_id,cs,(/1/),(/nvrt-kz+1/)))
      ztot(kz)= -h_s ! add last z level
      if(kz/=1) ir=CK(nf90_put_var(ncid,iz_id,ztot,(/1/),(/kz/)))
      ir=CK(nf90_put_var(ncid,ihc_id,h_c))
      ir=CK(nf90_put_var(ncid,ih0_id,h0))
    endif !inc
 
    !print*, 'Last element:',nm(ne_global,1:3)
    !end output header

    ! Loop over output spools in file
    do ispool=1,nrec

      !Gather all ranks
      do irank=0,nproc-1
        !Open input file
        fgb2=fgb
        write(fgb2(lfgb-3:lfgb),'(i4.4)') irank

        open(63,file=fgb2(1:lfgb)//'_'//file63,access='direct',recl=ihot_len(irank),status='old')
 
        if(itype==2) then
          read(63,rec=ispool)time,it,(eta2(iplg(irank,i)),i=1,np(irank)),((outb(iplg(irank,i),1,m),m=1,ivs),i=1,np(irank))
        else if(itype==3) then !3D
          read(63,rec=ispool)time,it,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (((outb(iplg(irank,i),k,m),m=1,ivs),k=max0(1,kbp01(irank,i)),nvrt),i=1,np(irank))
        else if(itype==4) then !2D side
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (outsb(islg(irank,i),1,1:ivs),i=1,ns(irank))
        else if(itype==5) then !2D elem
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (outeb(ielg(irank,i),1,1:ivs),i=1,ne(irank))
        else if(itype==6.or.itype==7) then !3D side and whole/half level
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (((outsb(islg(irank,i),k,m),m=1,ivs),k=1,nvrt),i=1,ns(irank))
        else if(itype==8.or.itype==9) then !3D element at whole/half levels
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (((outeb(ielg(irank,i),k,m),m=1,ivs),k=1,nvrt),i=1,ne(irank))
        else if(itype==10) then !2D node (n.s.)
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &((outb(iplg(irank,i),1,m),m=1,ivs),i=1,np(irank))
        else !3D node
          read(63,rec=ispool)time,it,itmp,(eta2(iplg(irank,i)),i=1,np(irank)), &
     &                       (((outb(iplg(irank,i),k,m),m=1,ivs),k=1,nvrt),i=1,np(irank))
        endif
        ! Close input file
        close(63)
      enddo !irank

      if (data_loc=='side' .and. i23d_out==3) then
        ! mask out values below bottom
        do i=1,ns_global
          do k = 1,kbs(i)-1
            outsb(i,k,:) = -9999.
          enddo
        enddo
      else if (data_loc=='cntr' .and. i23d_out==3) then
        ! mask out values below bottom
        do i=1,ne_global
          do k = 1,kbe(i)-1
            outeb(i,k,:) = -9999.
          enddo
        enddo
      endif
      !Output
      !print*, 'it=',it,' time=',time/86400.0

      !Compute eta2 at sides/elem.
      do i=1,ns_global
        n1=isidenode(i,1); n2=isidenode(i,2)
        eta2s(i)=(eta2(n1)+eta2(n2))/2
      enddo !i
      do i=1,ne_global
        eta2e(i)=sum(eta2(nm(i,1:3)))/3
      enddo !i

      if(inc==0) then !binary

        a_4 = transfer(source=time,mold=a_4)
        write(65,"(a4)",advance="no") a_4
        a_4 = transfer(source=it,mold=a_4)
        write(65,"(a4)",advance="no") a_4
      
        if(itype<4.or.itype>=10) then !node based
          do i=1,np_global
            a_4 = transfer(source=eta2(i),mold=a_4)
            write(65,"(a4)",advance="no") a_4
          enddo !i
          do i=1,np_global
            if(itype==2.or.itype==10) then
              do m=1,ivs
                a_4 = transfer(source=outb(i,1,m),mold=a_4)
                write(65,"(a4)",advance="no") a_4
              enddo !m
            else !if(itype==3) then
              do k=max0(1,kbp00(i)),nvrt
                do m=1,ivs
                  a_4 = transfer(source=outb(i,k,m),mold=a_4)
                  write(65,"(a4)",advance="no") a_4
                enddo !m
              enddo !k
            endif !itype
          enddo !i
        else if(itype==4) then !2D side
          do i=1,ns_global
            a_4 = transfer(source=eta2s(i),mold=a_4)
            write(65,"(a4)",advance="no") a_4
          enddo !i
          do i=1,ns_global
            do m=1,ivs
              a_4 = transfer(source=outsb(i,1,m),mold=a_4)
              write(65,"(a4)",advance="no") a_4
            enddo !m
          enddo !i
        else if(itype==5) then !2D elem
          do i=1,ne_global
            a_4 = transfer(source=eta2e(i),mold=a_4)
            write(65,"(a4)",advance="no") a_4
          enddo !i
          do i=1,ne_global
            do m=1,ivs
              a_4 = transfer(source=outeb(i,1,m),mold=a_4)
              write(65,"(a4)",advance="no") a_4
            enddo !m
          enddo !i
        else if(itype==6.or.itype==7) then !3D side and whole/half level
          do i=1,ns_global
            a_4 = transfer(source=eta2s(i),mold=a_4)
            write(65,"(a4)",advance="no") a_4
          enddo !i
          do i=1,ns_global
            do k=max0(1,kbs(i)),nvrt
              do m=1,ivs
                a_4 = transfer(source=outsb(i,k,m),mold=a_4)
                write(65,"(a4)",advance="no") a_4
              enddo !m
            enddo !k 

            !Debug
            !if(iinput==iend.and.ispool==nrec) then
            !  write(89,*)xcj(i),ycj(i),outsb(i,nvrt,1:ivs)
            !endif

          enddo !i
        else !3D element and whole/half level
          do i=1,ne_global
            a_4 = transfer(source=eta2e(i),mold=a_4)
            write(65,"(a4)",advance="no") a_4
          enddo !i
          do i=1,ne_global
            do k=max0(1,kbe(i)),nvrt
              do m=1,ivs
                a_4 = transfer(source=outeb(i,k,m),mold=a_4)
                write(65,"(a4)",advance="no") a_4
              enddo !m
            enddo !k 
          enddo !i
        endif !itype
      else !netcdf
        ! reduce data precision
        if (ndigits > -99) then
          if (data_loc=='node') then
            if ( i23d_out==2 ) then
              do i=1,np_global
                do m=1,ivs
                  outb(i,1,m) = round(outb(i,1,m),ndigits)
                enddo
              enddo
            else ! 3D
              do i=1,np_global
                do k=max0(1,kbp00(i)),nvrt
                  do m=1,ivs
                    outb(i,k,m) = round(outb(i,k,m),ndigits)
                  enddo
                enddo
              enddo
            endif ! i23d_out
          else if (data_loc=='side') then
            if ( i23d_out==2 ) then
              do i=1,ns_global
                do m=1,ivs
                  outsb(i,1,m) = round(outsb(i,1,m),ndigits)
                enddo
              enddo
            else ! 3D
              do i=1,ns_global
                do k=max0(1,kbs(i)),nvrt
                  do m=1,ivs
                    outsb(i,k,m) = round(outsb(i,k,m),ndigits)
                  enddo
                enddo
              enddo
            endif ! i23d_out
          else if (data_loc=='cntr') then
            if ( i23d_out==2 ) then
              do i=1,ne_global
                do m=1,ivs
                  outeb(i,1,m) = round(outeb(i,1,m),ndigits)
                enddo
              enddo
            else ! 3D
              do i=1,ne_global
                do k=max0(1,kbe(i)),nvrt
                  do m=1,ivs
                    outeb(i,k,m) = round(outeb(i,k,m),ndigits)
                  enddo
                enddo
              enddo
            endif ! i23d_out
          endif ! data_loc
        endif ! ndigits

        ! write time
        ir=CK(nf90_put_var(ncid,itime_id,time,(/ ispool /)))
        ! write elev
        data_start_2d(1)=1; data_start_2d(2)=ispool
        data_count_2d(2)=1
        if (data_loc=='node') then
          data_count_2d(1)=np_global
          ir=CK(nf90_put_var(ncid,ielev_id,eta2,data_start_2d,data_count_2d))
          if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
            ir=CK(nf90_put_var(ncid,ivar_id,outb(:,1,1),data_start_2d,data_count_2d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outb(:,1,2),data_start_2d,data_count_2d))
            endif
          endif
        else if (data_loc=='side') then
          data_count_2d(1)=ns_global
          ir=CK(nf90_put_var(ncid,ielev_id,eta2s,data_start_2d,data_count_2d))
          if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
            ir=CK(nf90_put_var(ncid,ivar_id,outsb(:,1,1),data_start_2d,data_count_2d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outsb(:,1,2),data_start_2d,data_count_2d))
            endif
          endif
        else if (data_loc=='cntr') then
          data_count_2d(1)=ne_global
          ir=CK(nf90_put_var(ncid,ielev_id,eta2e,data_start_2d,data_count_2d))
          if ( i23d_out==2 .and. file63(1:lfile63-3) /= 'elev' ) then
            ir=CK(nf90_put_var(ncid,ivar_id,outeb(:,1,1),data_start_2d,data_count_2d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outeb(:,1,2),data_start_2d,data_count_2d))
            endif
          endif
        endif
        if ( i23d_out==3 ) then !3D
          data_start_3d(1:2)=1; data_start_3d(3)=ispool
          data_count_3d(2)=nvrt; data_count_3d(3)=1
          if (data_loc=='node') then
            data_count_3d(1)=np_global
            ir=CK(nf90_put_var(ncid,ivar_id,outb(:,:,1),data_start_3d,data_count_3d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outb(:,:,2),data_start_3d,data_count_3d))
            endif
          else if (data_loc=='side') then
            data_count_3d(1)=ns_global
            ir=CK(nf90_put_var(ncid,ivar_id,outsb(:,:,1),data_start_3d,data_count_3d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outsb(:,:,2),data_start_3d,data_count_3d))
            endif
          else if (data_loc=='cntr') then
            data_count_3d(1)=ne_global
            ir=CK(nf90_put_var(ncid,ivar_id,outeb(:,:,1),data_start_3d,data_count_3d))
            if( ivs==2 ) then
              ir=CK(nf90_put_var(ncid,ivar2_id,outeb(:,:,2),data_start_3d,data_count_3d))
            endif
          endif
        endif !i23d_out
        print*, 'done record # ',ispool
      endif !inc

    enddo !ispool=1,nrec
  ! Close output file
    if(inc==0) then
      close(65)
    else
      iret = nf90_close(ncid)
    endif
  enddo !iinput

  stop
end program combine
