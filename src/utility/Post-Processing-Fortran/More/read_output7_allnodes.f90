!
!****************************************************************************************
!											*
!	Read binary format v5.0 (hybrid S-Z models) for multiple files at all nodes	*
!
!       Outputs: extract.out
!     History: (1) added non-standard outputs (April 2012) - transparent to most scripts
!               as format is same;
!****************************************************************************************
!     ifort -Bstatic -O2 -assume byterecl -o read_output7_allnodes read_output7_allnodes.f90
      program read_out
      parameter(nbyte=4)
!      parameter(mnp=80000)
!      parameter(mne=160000)
!      parameter(mnv=80)
      character*30 file63
      character*12 it_char
      character*48 start_time,version,variable_nm,variable_dim
      character*48 data_format
!      dimension sigma(mnv),cs(mnv),ztot(mnv),x(mnp),y(mnp),dp(mnp),kbp00(mnp),kfp(mnp)
!      dimension nm(mne,4),out(mnp,mnv,2),icum(mnp,mnv,2),eta2(mnp),ztmp(mnv)
      allocatable :: sigma(:),cs(:),ztot(:),x(:),y(:),dp(:),kbp00(:),kfp(:)
      allocatable :: nm(:,:),out(:,:,:),icum(:,:,:),eta2(:),ztmp(:,:)
      allocatable :: xcj(:),ycj(:),dps(:),kbs(:),xctr(:),yctr(:),dpe(:),kbe(:)
      allocatable :: zs(:,:),ze(:,:),idry(:),outs(:,:,:),oute(:,:,:)
      allocatable :: ic3(:,:),js(:,:),is(:,:),isidenode(:,:)
      
      print*, 'Input file to read from (without *_; e.g. salt.63):'
      read(*,'(a30)')file63
      
      print*, 'Input # of files (stacks)  to read:'
      read(*,*) ndays

      open(65,file='extract.out')
      
!...  Header
!...
      open(63,file='1_'//file63,status='old',access='direct',recl=nbyte)
      irec=0
      do m=1,48/nbyte
        read(63,rec=irec+m) data_format(nbyte*(m-1)+1:nbyte*m)
      enddo
      if(data_format.ne.'DataFormat v5.0') then
        print*, 'This code reads only v5.0:  ',data_format
        stop
      endif
      irec=irec+48/nbyte
      do m=1,48/nbyte
        read(63,rec=irec+m) version(nbyte*(m-1)+1:nbyte*m)
      enddo
      irec=irec+48/nbyte
      do m=1,48/nbyte
        read(63,rec=irec+m) start_time(nbyte*(m-1)+1:nbyte*m)
      enddo
      irec=irec+48/nbyte
      do m=1,48/nbyte
        read(63,rec=irec+m) variable_nm(nbyte*(m-1)+1:nbyte*m)
      enddo
      irec=irec+48/nbyte
      do m=1,48/nbyte
        read(63,rec=irec+m) variable_dim(nbyte*(m-1)+1:nbyte*m)
      enddo
      irec=irec+48/nbyte

      write(*,'(a48)')data_format
      write(*,'(a48)')version
      write(*,'(a48)')start_time
      write(*,'(a48)')variable_nm
      write(*,'(a48)')variable_dim

      read(63,rec=irec+1) nrec
      read(63,rec=irec+2) dtout
      read(63,rec=irec+3) nspool
      read(63,rec=irec+4) ivs
      read(63,rec=irec+5) i23d
      irec=irec+5

      print*, 'i23d=',i23d,' nrec= ',nrec

!     Vertical grid
      read(63,rec=irec+1) nvrt
      read(63,rec=irec+2) kz
      read(63,rec=irec+3) h0
      read(63,rec=irec+4) h_s
      read(63,rec=irec+5) h_c
      read(63,rec=irec+6) theta_b
      read(63,rec=irec+7) theta_f
      irec=irec+7
      allocate(ztot(nvrt),sigma(nvrt),cs(nvrt),stat=istat)
      if(istat/=0) stop 'Falied to allocate (1)'

      do k=1,kz-1
        read(63,rec=irec+k) ztot(k)
      enddo
      do k=kz,nvrt
        kin=k-kz+1
        read(63,rec=irec+k) sigma(kin)
        cs(kin)=(1-theta_b)*sinh(theta_f*sigma(kin))/sinh(theta_f)+ &
     &theta_b*(tanh(theta_f*(sigma(kin)+0.5))-tanh(theta_f*0.5))/2/tanh(theta_f*0.5)
      enddo
      irec=irec+nvrt

!     Horizontal grid
      read(63,rec=irec+1) np !could be ns,ne also
      read(63,rec=irec+2) ne
      irec=irec+2
      allocate(x(np),y(np),dp(np),kbp00(np),kfp(np),nm(ne,4),out(np,nvrt,2),idry(np), &
     !&icum(np,nvrt,2),eta2(np),stat=istat)
     &eta2(np),xctr(ne),yctr(ne),dpe(ne),kbe(ne),ztmp(nvrt,np),ze(nvrt,ne), &
     &oute(2,nvrt,ne),stat=istat)
      if(istat/=0) stop 'Falied to allocate (2)'

      do m=1,np
        read(63,rec=irec+1)x(m)
        read(63,rec=irec+2)y(m)
        read(63,rec=irec+3)dp(m)
        read(63,rec=irec+4)kbp00(m)
        irec=irec+4
      enddo !m=1,np

!     Additional for non-standard outputs
!      if(i23d==4) then !3D side @ whole levels
!        read(63,rec=irec+1) ns
!        irec=irec+1
!        allocate(xcj(ns),ycj(ns),dps(ns),kbs(ns),zs(nvrt,ns),outs(2,nvrt,ns),stat=istat)
!        if(istat/=0) stop 'Falied to allocate (5)'
!        do m=1,ns
!          read(63,rec=irec+1)xcj(m)
!          read(63,rec=irec+2)ycj(m)
!          read(63,rec=irec+3)dps(m)
!          read(63,rec=irec+4)kbs(m)
!          irec=irec+4
!        enddo !m
!      else if (i23d>4) then !3D elem. @ whole/half
!        read(63,rec=irec+1) netmp
!        irec=irec+1
!        do m=1,ne
!          read(63,rec=irec+1)xctr(m)
!          read(63,rec=irec+2)yctr(m)
!          read(63,rec=irec+3)dpe(m)
!          read(63,rec=irec+4)kbe(m)
!          irec=irec+4
!        enddo !m
!      endif !i23d

      do m=1,ne
        read(63,rec=irec+1)i34
        irec=irec+1
        do mm=1,i34
          read(63,rec=irec+1)nm(m,mm)
          irec=irec+1
        enddo !mm
      enddo !m
      irec0=irec

!     Compute geometry
!      call compute_nside(np,ne,nm,ns2)
!      if(.not.allocated(xcj)) allocate(xcj(ns2))
!      if(.not.allocated(ycj)) allocate(ycj(ns2))
!      allocate(ic3(ne,3),js(ne,3),is(ns2,2),isidenode(ns2,2),stat=istat)
!      if(istat/=0) stop 'Allocation error: side(0)'
!      call selfe_geometry(np,ne,ns2,x,y,nm,ic3,js,is,isidenode,xcj,ycj)

      print*, 'last element',(nm(ne,j),j=1,3)

!...  Compute relative record # for a node and level for 3D outputs
!...
!      icount=0
!      do i=1,np
!        do k=max0(1,kbp00(i)),nvrt
!          do m=1,ivs
!            icount=icount+1
!            icum(i,k,m)=icount
!          enddo !m
!        enddo !k
!      enddo !i=1,np

!...  Time iteration
!...
      it_tot=0
      out=-99 !init.
      ztmp=-99
      do iday=1,ndays
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      write(it_char,'(i12)')iday
      open(63,file=it_char//'_'//file63,status='old',access='direct',recl=nbyte)

      irec=irec0
      do it1=1,nrec
        read(63,rec=irec+1) time
        read(63,rec=irec+2) it
        irec=irec+2
        it_tot=it_tot+1
        time=it_tot*dtout

        print*, 'time in days =',time

        do i=1,np
          read(63,rec=irec+i) eta2(i)
        enddo !i
        irec=irec+np

!       Compute z coordinates
        do i=1,np
          if(eta2(i)+dp(i)<h0) then !node dry
            idry(i)=1
          else !wet
            idry(i)=0
!           Compute z-coordinates
            do k=kz,nvrt
              kin=k-kz+1
              hmod2=min(dp(i),h_s)
              if(hmod2<=h_c) then
                ztmp(k,i)=sigma(kin)*(hmod2+eta2(i))+eta2(i)
              else if(eta2(i)<=-h_c-(hmod2-h_c)*theta_f/sinh(theta_f)) then
                write(*,*)'Pls choose a larger h_c (2):',eta2(i),h_c
                stop
              else
                ztmp(k,i)=eta2(i)*(1+sigma(kin))+h_c*sigma(kin)+(hmod2-h_c)*cs(kin)
              endif

!             Following to prevent underflow
              if(k==kz) ztmp(k,i)=-hmod2
              if(k==nvrt) ztmp(k,i)=eta2(i)
            enddo !k

            if(dp(i)<=h_s) then
              kbpl=kz
            else !z levels
!             Find bottom index
              kbpl=0
              do k=1,kz-1
                if(-dp(i)>=ztot(k).and.-dp(i)<ztot(k+1)) then
                  kbpl=k
                  exit
                endif
              enddo !k
              if(kbpl==0) then
                write(*,*)'Cannot find a bottom level:',dp(i)
                stop
              endif
              ztmp(kbpl,i)=-dp(i)
              do k=kbpl+1,kz-1
                ztmp(k,i)=ztot(k)
              enddo !k
            endif
            if(kbpl/=kbp00(i)) stop 'Bottom index wrong'

            do k=kbpl+1,nvrt
              if(ztmp(k,i)-ztmp(k-1,i)<=0) then
                write(*,*)'Inverted z-level:',eta2(i),dp(i),ztmp(k,i)-ztmp(k-1,i)
                stop
              endif
            enddo !k
          endif !dry/wet
        enddo !i=1,np
          
        if(i23d==2) then !2D
          do i=1,np
            do m=1,ivs
              read(63,rec=irec+1) out(i,1,m)
              irec=irec+1
            enddo !m
          enddo !i
!         Output: time, 2D variable at all nodes
          write(65,*)time/86400,((out(i,1,m),m=1,ivs),i=1,np)
        else !if(i23d==3) then !3D 
          do i=1,np
            do k=max0(1,kbp00(i)),nvrt
              do m=1,ivs
                read(63,rec=irec+1) out(i,k,m)
                irec=irec+1
              enddo !m
            enddo !k
            do k=max0(1,kbp00(i)),nvrt
!             Output: time, node #, level #, z-coordinate, 3D variable 
              if(idry(i)==1) then
                write(65,*)time/86400,i,k,-99.,(out(i,k,m),m=1,ivs)
              else
                write(65,*)time/86400,i,k,ztmp(k,i),(out(i,k,m),m=1,ivs)
              endif
            enddo !k

            !Debug
            !write(65,*)x(i),y(i),out(i,1,1:ivs) 

          enddo !i
!        else if(i23d==4) then !3D side @ whole level
!          do i=1,ns
!            !Compute zcor
!            n1=isidenode(i,1); n2=isidenode(i,2)
!            zs(:,i)=(ztmp(:,n1)+ztmp(:,n2))/2
!            do k=max0(1,kbs(i)),nvrt
!              do m=1,ivs
!                read(63,rec=irec+1) outs(m,k,i)
!                irec=irec+1
!              enddo !m
!          
!              !Output
!              write(65,*)time/86400,i,k,zs(k,i),outs(1:ivs,k,i)
!            enddo !k
!          enddo !i
!        else !3D elem. @whole/half level
!          do i=1,ne
!            !Compute zcor
!            n1=nm(i,1); n2=nm(i,2); n3=nm(i,3)
!            ze(:,i)=(ztmp(:,n1)+ztmp(:,n2)+ztmp(:,n3))/3
!            do k=max0(1,kbe(i)),nvrt
!              do m=1,ivs
!                read(63,rec=irec+1) oute(m,k,i)
!                irec=irec+1
!              enddo !m
!
!              !Output
!              write(65,*)time/86400,i,k,ze(k,i),oute(1:ivs,k,i)
!            enddo !k
!          enddo !i
        endif !i23d
      enddo !it1=1,nrec

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      enddo !iday=1,ndays

      stop
      end
