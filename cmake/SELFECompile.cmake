


if("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "Intel")
    #message(STATUS "Overriding default cmake Intel compiler flags")
    set (SELFE_INTEL_OPTIONS "-assume byterecl")
    set( CMAKE_Fortran_FLAGS_RELEASE_INIT "-O2 ${SELFE_INTEL_OPTIONS}")
    set( CMAKE_Fortran_FLAGS_DEBUG_INIT "-g ${SELFE_INTEL_OPTIONS}")
    set( CMAKE_Fortran_FLAGS_RELWITHDEEBINFO_INIT "-O2 -g ${SELFE_INTEL_OPTIONS}")
endif()


