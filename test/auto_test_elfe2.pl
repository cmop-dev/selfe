#!/usr/bin/perl -w

# Auto testing of new SELFE versions
# Changes/history: removed plotting with matlab

# Before launch: copy all executibles to this dir 
# Use change_param.pl to adjust param.in for new versions
# Output messages from screen.
# For abnormal finish, kill all processes on sirius and qdel all queues.
# May have an optional argument to confine tests to a group
# of tests (with dir name match) 
# After done, use auto_test_elfe2.m to plot images.

#use Cwd;

if(@ARGV>1)
{ print "USAGE: $0  <optional dir name match, without />\n"; exit(1); }
#Echo
print "$0 @ARGV\n";

#$mc="canopus";
$mc="sirius";
$codeid="3.1kj"; #code name is pelfe_$codeid_sirius with possible extensions later
$code="pelfe_".$codeid."_".$mc;
# All tests; order them from least time-consuming to most
@tests=('Test_Btrack_Gausshill','Test_Btrack_Gausshill_CPU','Test_VolConsv_3D_1','Test_VolConsv_3D_2','Test_VolConsv_2D_1',
        'Test_VolConsv_2D_2', 'Test_HeatConsv_Upwind','Test_HeatConsv_TVD', 
        'Test_Inun_NWaves_3D','Test_Inun_NWaves_2D','Test_Inun_CircularIsland_CaseB',
        'Test_Inun_CircularIsland_CaseB_CPU','Test_Inun_CircularIsland_CaseB_3D','Test_QuarterAnnulus',
        'Test_QuarterAnnulus_hvis','Test_Convergence_Grid1','Test_Convergence_Grid2',
        'Test_Convergence_Grid3','Test_Convergence_Grid4','Test_Convergence_Grid5',
        'Test_Flat_Adjust','Test_Geostrophic','Test_Nonhydro_StandingWaves',
        'Test_Williamson5','Test_ECO_Toy',
        'Test_WWM_L51','Test_WWM_Analytical','Test_WWM_L31_1A',
        'Test_WWM_L31_2A','Test_WWM_L31_3A','Test_WWM_limon_NODIF',
        'Test_CORIE','Test_Nonhydro_Flat_Adjust');
#      *_CPU must have different # of CPUs from original test
@CPUs=(3,12,20,20,2,
       2,4,4,
       16,16,24,
       15,24,1,
       1,1,1,
       1,1,3,
       20,2,16,
       20,1,
       8,4,6,
       6,6,4,
       16,24);
#@start=(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
#end stack #
@end=(1,1,1,1,1,
      1,1,1,
      3,3,1,
      1,1,1,
      1,1,1,
      1,1,1,
      1,1,1,
      3,1,
      1,1,1,
      1,1,1,
      28,1);

#Input check
if(@tests != @CPUs || @CPUs != @end) {die "Wrong inputs\n";}

#$cdir=getcwd(); #current working dir

system "rm -rf Images/*";

#Launch runs
for($i=0;$i<@tests;$i++)
{
  $dir=$tests[$i];
  if(@ARGV==0 || $dir =~ "$ARGV[0]")
  {
    print "doing $dir\n";

    #Use proper code
    $ntracers=0;    
    if($dir =~ "_WWM_") 
      {$code2=$code."_WWM";}
    elsif($dir =~ "_ECO_")
      {$code2=$code."_ECO"; $ntracers=25;}
    else {$code2=$code;}

    system "cd $dir; rm -rf pelfe* outputs mirror.out tmp runhind.qsub.*; mkdir outputs; cp ../$code2 .";

    #Common additions to param.in (to be appended)

    #Change runhind.qsub
    open(IN,"<runhind.qsub"); @all=<IN>; close(IN);
    @last=split(" ",$all[@all-1]);
    $all[@all-1]="@last[0..@last-2] ./$code2\n";
    open(IN,">$dir/runhind.qsub"); print IN @all; close(IN);
    #Run SELFE
    if($mc =~ "sirius" )
      {system "cd $dir; qsub -q ib.q -pe orte $CPUs[$i] runhind.qsub";}
    elsif($mc =~ "canopus")
      {system "cd $dir; mpiexec -n $CPUs[$i] $code2 </dev/null > scrn.out 2>&1 &";}
    else
      {die "Unknow machine $mc\n"}

    #Combining
    sleep 200;
    print "Checking in $dir\n";
    if(!-e "$dir/mirror.out") 
    {  print "Run $dir did not launch...\n"; next;}
    else
    {
      @last=("Hey"); #init.
      while($last[@last-1] !~ "Run completed successfully at")
      {
        sleep 120;
        system "tail -n 1 $dir/mirror.out >& $dir/tmp";
        open(IN,"<$dir/tmp"); @last=<IN>; close(IN);
      } #while
      print "Run $dir is done...\n";
      system "cd $dir; ../autocombine_MPI_elfe.pl 1 $end[$i] >& screen.out";
      print "Done combining...\n";
    } #if

    #Post-processing
    if(-e "$dir/postpros.pl") 
    {
      system "cd $dir; ./postpros.pl 1 $end[$i] $dir >& postpros.out";
      print "Done post-processing in $dir...\n";
    } #if

    print "--------------------------------------\n";
  } # @ARGV
} #for

# Link images to web dir
#if($FL !=1) {
#  system "ln -sf $cdir/Images/* /home/users/yinglong/public_html/FTP_DIR/Auto_Tests/";
#}

print "Finished...\n";
