#!/usr/bin/perl -w

# Modify param.in for new version of SELFE
# Assume some rigidity in format (see below for details)
# Run in the same dir as "Test_*"

if(@ARGV>1)
{ print "USAGE: $0  <optional dir name match, without />\n"; exit(1); }
#Echo
print "$0 @ARGV\n";

@files = <*>;
for ($i=0;$i<@files;$i++) {
  $f = $files[$i];
  if(-d $f && $f =~ /Test_*/) {
#    print "doing $f/param.in\n";
    #Backup a copy
    system "cp -f $f/param.in $f/param.in.0";
    open(FL,"<$f/param.in.0"); @all=<FL>; close(FL); 
    #open(OUT,">$f/param.in");
    #@found[1..2]=0; #flag
    #for($j=0;$j<@all;$j++) {
    #  $out=$all[$j];
      #Replace
      #@str=split(" ",$all[$j]);
      #if(@str>2) {
      #  if($str[0] =~ /dtb_max1/ && $str[1] =~ /=/) {
      #    $found[1]=1;
      #    $tmp=$str[2]*1.e-4;
      #    $out="/dtb_max1/ = $tmp\n";
      #  } #if
      #} #if
      #Output
      #print OUT "$out";
    #} #for $j
    #if($found[1]==0) {die "WARNING: Cannot find the parameter in $f/param.in\n";}
    #close(OUT);

    #Append new parameters
    system "rm -f $f/tmp";
    open(NEW,">$f/tmp");
    print NEW "  ihydraulics = 0\n";
    close(NEW);
    system "cat $f/tmp >> $f/param.in";
  } #if -d $f
} #for

