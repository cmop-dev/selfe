function []=auto_test_elfe2(tid0)
% auto_test_elfe2(tid0)
% Plot results generated from auto_test_elfe2.pl
% tid0: optional argument for test run id (e.g. 'Test_Convergence_Grid')
% Make changes to each individual tests below

close all;
tid='Test_Btrack_Gausshill';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
cd(tid);
%Copy old results to Images/
copyfile('Gausshill_slab0.png','../Images/');
copyfile('Gausshill_sample0.png','../Images/');
%copyfile('Gausshill_slab0.avi','../Images/');

SELFE_SLAB('./','hgrid.gr3','temp.63','S',31,1,59,'n'); %generate slab.avi for surface T
movefile('slab.png','../Images/Gausshill_slab.png','f');
close all;
%Plot sample
SELFE_TRANSECT('./','hgrid.gr3','transect.bp','temp.63',1,1,'n'); 
movefile('trans.png','../Images/Gausshill_sample.png','f');

cd('../Test_Btrack_Gausshill_CPU');
SELFE_SLAB('./','hgrid.gr3','temp.63','S',31,1,59,'n'); 
movefile('slab.png','../Images/Gausshill_slab_CPU.png','f');
close all;
%Plot sample
SELFE_TRANSECT('./','hgrid.gr3','transect.bp','temp.63',1,1,'n');
movefile('trans.png','../Images/Gausshill_sample_CPU.png','f');

cd ..;
%----------------------------------------------------
end

close all;
tid='Test_Convergence_Grid';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta1=load(strcat(tid,'1/ForPlot_elev.dat'));
eta2=load(strcat(tid,'2/ForPlot_elev.dat'));
eta3=load(strcat(tid,'3/ForPlot_elev.dat'));
eta4=load(strcat(tid,'4/ForPlot_elev.dat'));
eta5=load(strcat(tid,'5/ForPlot_elev.dat'));
vel1=load(strcat(tid,'1/ForPlot_vel.dat')); %time (days), mag.
vel2=load(strcat(tid,'2/ForPlot_vel.dat'));
vel3=load(strcat(tid,'3/ForPlot_vel.dat'));
vel4=load(strcat(tid,'4/ForPlot_vel.dat'));
vel5=load(strcat(tid,'5/ForPlot_vel.dat'));
eta_ana=load(strcat(tid,'5/ana_elev.dat'));
vel_ana=load(strcat(tid,'5/ana_vel.dat'));

subplot(2,1,1);
plot(eta1(:,1),eta1(:,2),'k',eta2(:,1),eta2(:,2),'g',eta3(:,1),eta3(:,2),'b',...
     eta4(:,1),eta4(:,2),'m',eta5(:,1),eta5(:,2),'c',eta_ana(:,1),eta_ana(:,2),'r');
xlim([4.4 5]);
title({'Quarter annulus convergence test'; 'Elev.'});
legend('Grid1(3x4)','6x8','12x16','24x32','48x64',...
       'Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Elev. (m)');
subplot(2,1,2);
plot(vel1(:,1),vel1(:,2),'k',vel2(:,1),vel2(:,2),'g',vel3(:,1),vel3(:,2),'b',...
     vel4(:,1),vel4(:,2),'m',vel5(:,1),vel5(:,2),'c',vel_ana(:,1),vel_ana(:,2),'r');
xlim([4.4 4.8]);
title('Velocity');
legend('3x4','6x8','12x16','24x32','48x64',...
       'Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Vel. mag. (m/s)');
print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Flat_Adjust';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_salt.dat.RUN114')); %x,y,S
new=load(strcat(tid,'/ForPlot_salt.dat'));
indx_old=find(old(:,3)>9998);
indx_new=find(old(:,3)>9998);
old(indx_old,3)=NaN;
new(indx_new,3)=NaN;
%For contour plot
nz=41; nx=size(old,1)/nz;
yy=old(1:nz,2);
xx=old(1:nz:end,1);
if(length(xx) ~= nx); error('Wrong x dimension!'); end;
old2=reshape(old(:,3),nz,nx);
new2=reshape(new(:,3),nz,nx);

subplot(2,1,1);
[c,h]=contour(xx,yy,old2,6);
clabel(c,h), colorbar;
title({'Flat adjust';'RUN114: salt transect at t=0.5days'});
axis([2e4 4e4 -20 0]);
xlabel('x (m)'); ylabel('z (m)');
subplot(2,1,2);
[c,h]=contour(xx,yy,new2,6);
clabel(c,h), colorbar;
title('New results');
axis([2e4 4e4 -20 0]);
xlabel('x (m)'); ylabel('z (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Geostrophic';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
vel=load(strcat(tid,'/ForPlot_vel_prof.dat')); %x,u,v
np=length(vel(:,1));

subplot(2,1,1);
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'Geostrophic test (2D)';'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
subplot(2,1,2);
plot(1:np,vel(:,2),'b',1:np,vel(:,3),'k',1:np,0.98*ones(np,1),'r--');
title('Steady-state velocity: analytical=(0.98,0)');
legend('u','v','u_{ana}','Location','NorthEastOutside');
xlabel('Node #'); ylabel('m/s');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_HeatConsv_TVD';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2heat.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2heat.dat'));

subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'r');
title('Heat Consv. test: TVD: Old results');
legend('Numerical','Analytical','Location','NorthEastOutside');
ylabel('Total Heat');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'r');
title('New results');
legend('Numerical','Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Total Heat');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_HeatConsv_Upwind';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2heat.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2heat.dat'));
subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'r');
title('Heat Conv. test: Upwind: Old results');
legend('Numerical','Analytical','Location','NorthEastOutside');
ylabel('Total Heat');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'r');
title('New results');
legend('Numerical','Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Total Heat');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Hydraulic_Jump';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_old=load(strcat(tid,'/ForPlot_elev.ts.0')); %time, eta
eta_new=load(strcat(tid,'/ForPlot_elev.ts')); %time, eta
old=load(strcat(tid,'/ForPlot_elev_prof.dat.0')); %x,eta
new=load(strcat(tid,'/ForPlot_elev_prof.dat'));
bot=load(strcat(tid,'/ForPlot_bottom.dat')); %x,h
subplot(2,1,1);
plot(eta_old(:,1),eta_old(:,2),'g.',eta_new(:,1),eta_new(:,2),'b');
title({'Hydraulic jump over a weir',;'Elev. at a pt to check steady state'});
legend('Old result','New run');
xlabel('Time (days)'); ylabel('Elev.');
subplot(2,1,2);
plot(old(:,1),old(:,2),'g.',new(:,1),new(:,2),'b',bot(:,1),bot(:,2),'k');
title('Steady state');
legend('Old result','New run','Bottom');
xlabel('x (m)'); ylabel('Elev.');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Inun_CircularIsland_CaseB';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_runup_RUN64c')); %angle, runup
new=load(strcat(tid,'/ForPlot_runup.xy1'));
ana=load(strcat(tid,'/ForPlot_run2b.DATA'));
plot(old(:,1),old(:,2),'gd',new(:,1),new(:,2),'b.',ana(:,1),ana(:,2),'r.');
title('Solitary waves over a conical island: 2D model');
legend('Old result','New run','Obs');
xlabel('Angle (degr)'); ylabel('Max. runup');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Inun_CircularIsland_CaseB_3D';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_runup.xy1.0')); %angle, runup
new=load(strcat(tid,'/ForPlot_runup.xy1'));
ana=load(strcat(tid,'/ForPlot_run2b.DATA'));
plot(old(:,1),old(:,2),'gd',new(:,1),new(:,2),'b.',ana(:,1),ana(:,2),'r.');
title('Solitary waves over a conical island: 3D model');
legend('Old result','New run','Obs');
xlabel('Angle (degr)'); ylabel('Max. runup');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Inun_CircularIsland_CaseB_CPU';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_runup.xy1.1')); %angle, runup
new=load(strcat(tid,'/ForPlot_runup.xy1'));
ana=load(strcat(tid,'/ForPlot_run2b.DATA'));
plot(old(:,1),old(:,2),'gd',new(:,1),new(:,2),'b.',ana(:,1),ana(:,2),'r.');
title('Solitary waves over a conical island: comp. CPUs (2D model)');
legend('24 CPUs','15 CPUs','Obs');
xlabel('Angle (degr)'); ylabel('Max. runup');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Inun_NWaves_2D';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
shoreline_ana=load(strcat(tid,'/ForPlot_shoreline.ana')); %analytical soln: time, x,u
shoreline_new=load(strcat(tid,'/ForPlot_shoreline.dat')); %time(s), x,u
shoreline_old=load(strcat(tid,'/ForPlot_shoreline.RUN39b'));
shoreline_3D=load('Test_Inun_NWaves_3D/ForPlot_shoreline.dat'); %from 3D run
eta160_new=load(strcat(tid,'/ForPlot_t160.elev')); %time (s), eta
eta175_new=load(strcat(tid,'/ForPlot_t175.elev'));
eta220_new=load(strcat(tid,'/ForPlot_t220.elev'));
eta160_old=load(strcat(tid,'/ForPlot_t160.elev.RUN39b'));
eta175_old=load(strcat(tid,'/ForPlot_t175.elev.RUN39b'));
eta220_old=load(strcat(tid,'/ForPlot_t220.elev.RUN39b'));
eta160_3D=load('Test_Inun_NWaves_3D/ForPlot_t160.elev');
eta175_3D=load('Test_Inun_NWaves_3D/ForPlot_t175.elev');
eta220_3D=load('Test_Inun_NWaves_3D/ForPlot_t220.elev');
u160_new=load(strcat(tid,'/ForPlot_t160.u')); %time (s), u
u175_new=load(strcat(tid,'/ForPlot_t175.u')); 
u220_new=load(strcat(tid,'/ForPlot_t220.u')); 
u160_old=load(strcat(tid,'/ForPlot_t160.u.RUN39b')); 
u175_old=load(strcat(tid,'/ForPlot_t175.u.RUN39b')); 
u220_old=load(strcat(tid,'/ForPlot_t220.u.RUN39b')); 
u160_3D=load('Test_Inun_NWaves_3D/ForPlot_t160.u'); 
u175_3D=load('Test_Inun_NWaves_3D/ForPlot_t175.u'); 
u220_3D=load('Test_Inun_NWaves_3D/ForPlot_t220.u'); 
bot=load(strcat(tid,'/ForPlot_bottom.dat')); %x,h
eta160_ana=load(strcat(tid,'/ForPlot_t160.ana')); %time, eta, u
eta175_ana=load(strcat(tid,'/ForPlot_t175.ana'));
eta220_ana=load(strcat(tid,'/ForPlot_t220.ana'));
axis_prof=[-300 800 -40 30];

subplot(3,2,1); 
plot(eta160_new(:,1),eta160_new(:,2),'b',eta160_old(:,1),eta160_old(:,2),'g--',...
     eta160_3D(:,1),eta160_3D(:,2),'m--',eta160_ana(:,1),eta160_ana(:,2),'r',bot(:,1),bot(:,2),'k');
title('N waves runup: Elev. profiles: t=160s');
legend('New','Old','3D','Analytical','Bottom','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Elev.');
axis(axis_prof);
subplot(3,2,3); 
plot(eta175_new(:,1),eta175_new(:,2),'b',eta175_old(:,1),eta175_old(:,2),'g--',...
     eta175_3D(:,1),eta175_3D(:,2),'m--',eta175_ana(:,1),eta175_ana(:,2),'r',bot(:,1),bot(:,2),'k');
title('t=175s');
xlabel('x (m)'); ylabel('Elev.');
axis([200 300 -30 -5]);
subplot(3,2,5); 
plot(eta220_new(:,1),eta220_new(:,2),'b',eta220_old(:,1),eta220_old(:,2),'g--',...
     eta220_3D(:,1),eta220_3D(:,2),'m--',eta220_ana(:,1),eta220_ana(:,2),'r',bot(:,1),bot(:,2),'k');
title('t=220s');
xlabel('x (m)'); ylabel('Elev.');
axis([-160 -80 10 20]);

subplot(3,2,2);
plot(u160_new(:,1),u160_new(:,2),'b',u160_old(:,1),u160_old(:,2),'g--',...
     u160_3D(:,1),u160_3D(:,2),'m--',eta160_ana(:,1),eta160_ana(:,3),'r');
title('Velocity: t=160s');
xlabel('x (m)'); ylabel('U (m/s)');
axis([150 600 -5 8]);
subplot(3,2,4);
plot(u175_new(:,1),u175_new(:,2),'b',u175_old(:,1),u175_old(:,2),'g--',...
     u175_3D(:,1),u175_3D(:,2),'m--',eta175_ana(:,1),eta175_ana(:,3),'r');
title('Velocity: t=175s');
xlabel('x (m)'); ylabel('U (m/s)');
axis([200 400 -16 0]);
subplot(3,2,6);
plot(u220_new(:,1),u220_new(:,2),'b',u220_old(:,1),u220_old(:,2),'g--',...
     u220_3D(:,1),u220_3D(:,2),'m--',eta220_ana(:,1),eta220_ana(:,3),'r');
title('Velocity: t=220s');
xlabel('x (m)'); ylabel('U (m/s)');
axis([-150 -20 -2 2]);

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Nonhydro_StandingWaves';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_elev.dat.0')); %time (days), eta
new=load(strcat(tid,'/ForPlot_elev.dat'));
ana=load(strcat(tid,'/ForPlot_ana_10m.dat'));
plot(old(:,1)*86400,old(:,2),'bd',new(:,1)*86400,new(:,2),'g',ana(:,1)*86400,ana(:,2),'r');
title('Nondryo: standing waves');
legend('Old','New','Analytical');
xlabel('Time (sec)');
ylabel('Elev. (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_QuarterAnnulus_hvis';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_old=load(strcat(tid,'/ForPlot_elev.dat.0')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev.dat'));
eta_ana=load(strcat(tid,'/ForPlot_ana_elev.dat'));
vel_new=load(strcat(tid,'/ForPlot_vel.dat')); %time (days), mag.
vel_ana=load(strcat(tid,'/ForPlot_ana_vel.dat'));

subplot(2,1,1);
plot(eta_old(:,1),eta_old(:,2),'b.',eta_ana(:,1),eta_ana(:,2),'r',eta_new(:,1),eta_new(:,2),'g');
axis([4 5 -0.5 0.5]);
title({'Quarter annulus test (with hvis)';'elev.'});
legend('Old','Analytical','New','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Elev. (m)');
subplot(2,1,2);
plot(vel_new(:,1),vel_new(:,2),'g',vel_ana(:,1),vel_ana(:,2),'r');
axis([4 5 0 0.15]);
title('Velocity');
legend('New','Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Vel. mag. (m/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_QuarterAnnulus';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_old=load(strcat(tid,'/ForPlot_elev.dat.0')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev.dat'));
eta_ana=load(strcat(tid,'/ForPlot_ana_elev.dat'));
vel_old=load(strcat(tid,'/ForPlot_vel.dat.0')); %time (days), mag.
vel_new=load(strcat(tid,'/ForPlot_vel.dat')); %time (days), mag.
vel_ana=load(strcat(tid,'/ForPlot_ana_vel.dat'));

subplot(2,1,1);
plot(eta_old(:,1),eta_old(:,2),'b.',eta_ana(:,1),eta_ana(:,2),'r',eta_new(:,1),eta_new(:,2),'g');
axis([4 5 -0.5 0.5]);
title({'Quarter annulus test'; 'Elev.'});
legend('Old','Analytical','New','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Elev. (m)');
subplot(2,1,2);
plot(vel_new(:,1),vel_new(:,2),'g',vel_ana(:,1),vel_ana(:,2),'r',vel_old(:,1),vel_old(:,2),'b.');
axis([4 5 0 0.15]);
title('Velocity');
legend('New','Analytical','Old','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Vel. mag. (m/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_VolConsv_2D_1';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2fluxes.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2fluxes.dat'));

subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'g',[old(1,1) old(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('Volume consv. test: 2D, indvel=0: old results');
legend('Transect 1','Transect 2','Analytical');
ylabel('Flow (m^3/s)');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'g',[new(1,1) new(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('New results');
legend('Transect 1','Transect 2','Analytical');
xlabel('Time (days)'); ylabel('Flow (m^3/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_VolConsv_2D_2';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2fluxes.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2fluxes.dat'));

subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'g',[old(1,1) old(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('Volume consv. test: 2D, indvel=1: old results');
legend('Transect 1','Transect 2','Analytical');
ylabel('Flow (m^3/s)');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'g',[new(1,1) new(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('New results');
legend('Transect 1','Transect 2','Analytical');
xlabel('Time (days)'); ylabel('Flow (m^3/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_VolConsv_3D_1';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2fluxes.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2fluxes.dat'));

subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'g',[old(1,1) old(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('Volume consv. test: 3D, indvel=0: old results');
legend('Transect 1','Transect 2','Analytical');
ylabel('Flow (m^3/s)');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'g',[new(1,1) new(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('New results');
legend('Transect 1','Transect 2','Analytical');
xlabel('Time (days)'); ylabel('Flow (m^3/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_VolConsv_3D_2';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_2fluxes.dat.0')); %time, num., analytical
new=load(strcat(tid,'/ForPlot_2fluxes.dat'));

subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'g',[old(1,1) old(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('Volume consv. test: 3D, indvel=1: old results');
legend('Transect 1','Transect 2','Analytical');
ylabel('Flow (m^3/s)');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'g',[new(1,1) new(end,1)],[1.e4 1.e4],'r');
v=axis; axis([v(1) v(2) 9e3 11e3]);
title('New results');
legend('Transect 1','Transect 2','Analytical');
xlabel('Time (days)'); ylabel('Flow (m^3/s)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Williamson5';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_old=load(strcat(tid,'/ForPlot_elev_day5.xyz.0')); %x(ne,3),y(ne,3),eta(ne,3)
eta_new=load(strcat(tid,'/ForPlot_elev_day5.xyz'));
table=load(strcat(tid,'/ForPlot_table')); %n1,n2,n3

subplot(2,1,1);
%scatter(eta_old(:,2),eta_old(:,3),5,eta_old(:,4));
patch('Faces',table,'Vertices',eta_old(:,1:2),...
      'FaceVertexCData',eta_old(:,3),'FaceColor','interp','EdgeColor','none');
colormap(jet(15));
colorbar;
title('Old elev.');
subplot(2,1,2);
patch('Faces',table,'Vertices',eta_new(:,1:2),...
      'FaceVertexCData',eta_new(:,3),'FaceColor','interp','EdgeColor','none');
colormap(jet(15));
colorbar;
title('new elev.');
xlabel('Longitude'); ylabel('Latitude');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_Analytical';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev_prof.dat')); %x, eta
eta_old=load(strcat(tid,'/ForPlot_elev_prof.dat.0')); %x, eta
eta_obs=load(strcat(tid,'/ForPlot_ana_eta.dat')); %x, eta - analytical soln
Hs_new=load(strcat(tid,'/ForPlot_Hs_prof.dat')); %x, Hsig
Hs_old=load(strcat(tid,'/ForPlot_Hs_prof.dat.0')); 
Hs_obs=load(strcat(tid,'/ForPlot_ana_Hs.dat')); 

subplot(3,1,1);
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'Analytical test of Longuet-Higgins'; 'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
subplot(3,1,2);
plot(eta_old(:,1),eta_old(:,2),'b.',eta_new(:,1),eta_new(:,2),'g',eta_obs(:,1),eta_obs(:,2),'r');
title('Comp. of steady-state profiles: set-up');
%v=axis; axis([0 30 v(3) v(4)]);
legend('Old','New','Ana','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Elev. (m)');
subplot(3,1,3);
plot(Hs_old(:,1),Hs_old(:,2),'b.',Hs_new(:,1),Hs_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,2),'r');
title('Comp. of steady-state profiles: sig. wave height');
%v=axis; axis([0 30 v(3) v(4)]);
legend('Old','New','Ana','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Hs (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_L31_1A';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev_prof.dat')); %x, eta
eta_old=load(strcat(tid,'/ForPlot_elev_prof.dat.0')); %x, eta
eta_obs=load(strcat(tid,'/ForPlot_l31set1a.dat')); %x, eta (obs. data)
Hs_new=load(strcat(tid,'/ForPlot_Hs_prof.dat')); %x, Hsig
Hs_old=load(strcat(tid,'/ForPlot_Hs_prof.dat.0')); 
Hs_obs=load(strcat(tid,'/ForPlot_Hs_1a.dat'));  %obs.

subplot(3,1,1);
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'L31 test (1A)'; 'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
subplot(3,1,2);
plot(eta_old(:,1),eta_old(:,2),'b--',eta_new(:,1),eta_new(:,2),'g',eta_obs(:,1),eta_obs(:,2),'r');
title('Comp. of steady-state profiles: set-up');
v=axis; axis([0 30 -0.01 0.015]);
legend('Old','New','Obs','Location','NorthWest');
xlabel('x (m)'); ylabel('Elev. (m)');
subplot(3,1,3);
plot(Hs_old(:,1),Hs_old(:,2),'b--',Hs_new(:,1),Hs_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,2),'r');
title('Comp. of steady-state profiles: sig. wave height');
v=axis; axis([0 30 v(3) v(4)]);
legend('Old','New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Hs (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_L31_2A';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev_prof.dat')); %x, eta
eta_old=load(strcat(tid,'/ForPlot_elev_prof.dat.0')); %x, eta
eta_obs=load(strcat(tid,'/ForPlot_l31set2a.dat')); %x, eta (obs. data)
Hs_new=load(strcat(tid,'/ForPlot_Hs_prof.dat')); %x, Hsig
Hs_old=load(strcat(tid,'/ForPlot_Hs_prof.dat.0')); 
Hs_obs=load(strcat(tid,'/ForPlot_Hs_2a.dat'));  %obs.; x,Hs,Tp, TM01,TM02
TM01_new=load(strcat(tid,'/ForPlot_TM01_prof.dat')); %x,TM01
TM02_new=load(strcat(tid,'/ForPlot_TM02_prof.dat')); %x,TM02
Tp_new=load(strcat(tid,'/ForPlot_Tp_prof.dat')); %x,Tp

subplot(2,3,1);
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'L31 test (2A)'; 'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
subplot(2,3,2);
plot(eta_old(:,1),eta_old(:,2),'b--',eta_new(:,1),eta_new(:,2),'g',eta_obs(:,1),eta_obs(:,2),'r');
title('Comp. of steady-state profiles: set-up');
v=axis; axis([0 30 -0.01 0.015]);
legend('Old','New','Obs','Location','NorthWest');
xlabel('x (m)'); ylabel('Elev. (m)');
subplot(2,3,3);
plot(Hs_old(:,1),Hs_old(:,2),'b--',Hs_new(:,1),Hs_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,2),'r');
title('Comp. of steady-state profiles: sig. wave height');
v=axis; axis([0 30 v(3) v(4)]);
%legend('Old','New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Hs (m)');
subplot(2,3,4);
plot(TM01_new(:,1),TM01_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,4),'r');
title('Comp. of steady-state profiles: TM01');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('TM01 (sec)');
subplot(2,3,5);
plot(TM02_new(:,1),TM02_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,5),'r');
title('Comp. of steady-state profiles: TM02');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('TM02 (sec)');
subplot(2,3,6);
plot(Tp_new(:,1),Tp_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,3),'r');
title('Comp. of steady-state profiles: Tp');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Tp (sec)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_L31_3A';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
eta_new=load(strcat(tid,'/ForPlot_elev_prof.dat')); %x, eta
eta_old=load(strcat(tid,'/ForPlot_elev_prof.dat.0')); %x, eta
eta_obs=load(strcat(tid,'/ForPlot_l31set3a.dat')); %x, eta (obs. data)
Hs_new=load(strcat(tid,'/ForPlot_Hs_prof.dat')); %x, Hsig
Hs_old=load(strcat(tid,'/ForPlot_Hs_prof.dat.0')); 
Hs_obs=load(strcat(tid,'/ForPlot_Hs_3a.dat'));  %obs: x,Hs,Tp, TM01,TM02
TM01_new=load(strcat(tid,'/ForPlot_TM01_prof.dat')); %x,TM01
TM02_new=load(strcat(tid,'/ForPlot_TM02_prof.dat')); %x,TM02
Tp_new=load(strcat(tid,'/ForPlot_Tp_prof.dat')); %x,Tp

subplot(2,3,1);
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'L31 test (3A)'; 'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
subplot(2,3,2);
plot(eta_old(:,1),eta_old(:,2),'b--',eta_new(:,1),eta_new(:,2),'g',eta_obs(:,1),eta_obs(:,2),'r');
title('Comp. of steady-state profiles: set-up');
v=axis; axis([0 30 -0.01 0.015]);
legend('Old','New','Obs','Location','NorthWest');
xlabel('x (m)'); ylabel('Elev. (m)');
subplot(2,3,3);
plot(Hs_old(:,1),Hs_old(:,2),'b--',Hs_new(:,1),Hs_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,2),'r');
title('Comp. of steady-state profiles: sig. wave height');
v=axis; axis([0 30 v(3) v(4)]);
%legend('Old','New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Hs (m)');
subplot(2,3,4);
plot(TM01_new(:,1),TM01_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,4),'r');
title('Comp. of steady-state profiles: TM01');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('TM01 (sec)');
subplot(2,3,5);
plot(TM02_new(:,1),TM02_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,5),'r');
title('Comp. of steady-state profiles: TM02');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('TM02 (sec)');
subplot(2,3,6);
plot(Tp_new(:,1),Tp_new(:,2),'g',Hs_obs(:,1),Hs_obs(:,3),'r');
title('Comp. of steady-state profiles: Tp');
xlim([0 30]);
%legend('New','Obs','Location','NorthEastOutside');
xlabel('x (m)'); ylabel('Tp (sec)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_L51';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_new=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
vel_new=load(strcat(tid,'/ForPlot_curanddep.dat')); %x,y,u,v
vel_old=load(strcat(tid,'/ForPlot_curanddep.dat.0')); 
vel_obs=load(strcat(tid,'/ForPlot_curanddep.DATA.cleaned'));

subplot(2,2,1);
plot(eta_new(:,1),eta_new(:,2),'k');
title({'L51 test'; 'convergence to steady state'});
xlabel('Time (days)'); ylabel('Elev. (m)');

subplot(2,2,2); hold on;
quiver([vel_new(:,1)' -4],[vel_new(:,2)' 18],[vel_new(:,3)' 0.1],[vel_new(:,4)' 0],1);
axis([-10 30 -20 20]);
text(-4,20,'0.1m/s');
title('Velocity steady state: new');
xlabel('x (m)'); ylabel('y (m)');

subplot(2,2,3); hold on;
quiver([vel_old(:,1)' -4],[vel_old(:,2)' 18],[vel_old(:,3)' 0.1],[vel_old(:,4)' 0],1);
text(-4,20,'0.1m/s');
axis([-10 30 -20 20]);
title('Velocity steady state: old');
xlabel('x (m)'); ylabel('y (m)');

subplot(2,2,4); hold on;
quiver([vel_obs(:,1)' -4],[vel_obs(:,2)' 18],[vel_obs(:,3)' 0.1],[vel_obs(:,4)' 0],1);
text(-4,20,'0.1m/s');
axis([-10 30 -20 20]);
title('Velocity: observation');
xlabel('x (m)'); ylabel('y (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_ECO_Toy';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
p_old=load(strcat(tid,'/ForPlot_phyto.dat.0')); %time (days), eta
p_new=load(strcat(tid,'/ForPlot_phyto.dat'));
z_old=load(strcat(tid,'/ForPlot_zoo.dat.0')); 
z_new=load(strcat(tid,'/ForPlot_zoo.dat')); 

plot(p_old(:,1),p_old(:,2),'r',p_new(:,1),p_new(:,2),'k--',...
     z_old(:,1),z_old(:,2),'g',z_new(:,1),z_new(:,2),'b--');
title('BioToy test');
legend('Phyto(old)','Phyto(new)','Zoo(old)','Zoo(new)','Location','NorthEastOutside');
xlabel('Time (days)'); 

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_Nonhydro_Flat_Adjust';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
old=load(strcat(tid,'/ForPlot_salt.dat.RUN40zn')); %x,y,S
new=load(strcat(tid,'/ForPlot_salt.dat'));
indx_old=find(old(:,3)>9998);
indx_new=find(new(:,3)>9998);
old(indx_old,3)=NaN;
new(indx_new,3)=NaN;
%For contour plot
nz=151; nx=size(old,1)/nz;
yy=old(1:nz,2);
xx=old(1:nz:end,1);
if(length(xx) ~= nx); error('Wrong x dimension!'); end;
old2=reshape(old(:,3),nz,nx);
new2=reshape(new(:,3),nz,nx);

colormap('jet(8)');
subplot(2,1,1);
[c,h]=contourf(xx,yy,old2,40);
caxis([0 1.2]);
set(h,'edgecolor','none');
colorbar;
title({'Flat adjust (nohydro';'RUN40zn: salt transect at t=120s'});
%axis([2e4 4e4 -20 0]);
xlabel('x (m)'); ylabel('z (m)');
subplot(2,1,2);
[c,h]=contourf(xx,yy,new2,40);
caxis([0 1.2]);
set(h,'edgecolor','none');
colorbar;
title('New results');
%axis([2e4 4e4 -20 0]);
xlabel('x (m)'); ylabel('z (m)');

print('-dpng',strcat('Images/',tid,'.png'));
%----------------------------------------------------
end

close all;
tid='Test_WWM_limon_NODIF';
if(nargin==0 || length(regexp(tid,tid0)) ~=0)
%----------------------------------------------------
eta_ts=load(strcat(tid,'/ForPlot_elev.dat')); %time (days), eta
plot(eta_ts(:,1),eta_ts(:,2),'k');
title({'Limon test without diffraction'; 'Convergence to steady state'});
xlabel('Time (days)');ylabel('Elev.');
print('-dpng',strcat('Images/',tid,'.png'));

cd(tid);
%Copy old results to Images/
copyfile('Test_WWM_limon_NODIF_Hs_ref.png','../Images/');
copyfile('Test_WWM_limon_NODIF_hvel_ref.png','../Images/');

SELFE_SLAB('./','hgrid.gr3','wwm_1.61','S',9,1,431,'n');
movefile('limon.png','../Images/Test_WWM_limon_NODIF_Hs.png','f');
close all;
SELFE_SLAB('./','hgrid.gr3','hvel.64','S',9,1,431,'n');
movefile('limon.png','../Images/Test_WWM_limon_NODIF_hvel.png','f');
close all;

cd ..;
%----------------------------------------------------
end

